﻿#Przykład zapisania krzywej temperatury przez wyliczenie temperatur min i max na podstawie odchyłki dolnej
#i górnej otrzymanej z podanego pliku. Cmdlet nie obsługuje skryptów, dlatego musimy w pętli foreach obliczyć odpowiednie wartości
#i podać je bezpośrednio.
Param
(
    [string] $Id,
    [DateTime] $startCurveDate,
    [double] $baseTemperature,
    [string] $curveFile
)
Clear-Host;
$dane=$null;
$curveId=(Get-Date).Date - $startCurveDate;
Add-Type @"
public struct PeriodDeviatio
{
    public int CurveDay;
    public int PeriodNumber;
    public string StartPeriodTime;
    public string FinishPeriodTime;
    public double MinDeviatio;
    public double MaxDeviatio;
}
"@
Filter Convert-CsvToPeriod([Parameter(Mandatory = $true, ValueFromPipeline = $true)]$csvPeriod)
{
    $period = New-Object -TypeName PeriodDeviatio;
    $period.CurveDay = $csvPeriod.CurveDay;
    $period.PeriodNumber = $csvPeriod.PeriodNumber;
    $period.StartPeriodTime = $csvPeriod.StartPeriodTime;
    $period.FinishPeriodTime = $csvPeriod.FinishPeriodTime;
    $period.MinDeviatio = $csvPeriod.MinDeviatio;
    $period.MaxDeviatio = $csvPeriod.MaxDeviatio;
    $period;
}
$curveFile;
$dane=import-csv -delimiter ';' $curveFile | Convert-CsvToPeriod | Where-Object -FilterScript {$curveId.Days -eq $_.CurveDay}
foreach($d in $dane)
{
    $minTemperature = $baseTemperature - $d.MinDeviatio;
    $maxTemperature = $baseTemperature + $d.MaxDeviatio;
    Set-WitControllerSetting02  -Id $Id -TemperatureCurve -PeriodNumber $d.PeriodNumber -StartPeriodTime $d.StartPeriodTime -FinishPeriodTime $d.FinishPeriodTime -MinTemperature $minTemperature -MaxTemperature $maxTemperature;
}
