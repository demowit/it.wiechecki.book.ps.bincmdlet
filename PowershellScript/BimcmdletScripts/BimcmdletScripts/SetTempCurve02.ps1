﻿#Przykład wykorzystania cmdletu Set-WitControllerSetting03 do ustalenia dobowej krzywej temperatury.
#Cmdlet umożliwia przyjmowanie danych z poroku ByValueName i tak to wykorzystujemy
Param
(
    [string] $Id,
    [DateTime] $startCurveDate,
    [string] $curveFile
)
Clear-Host;
$curveId = (Get-Date).Date - $startCurveDate;
Add-Type @"
public struct PeriodData
{
    public int CurveDay;
    public int PeriodNumber;
    public string StartPeriodTime;
    public string FinishPeriodTime;
    public double MinTemperature;
    public double MaxTemperature;
}
"@
Filter Convert-CsvToPeriod([Parameter(Mandatory = $true, ValueFromPipeline = $true)]$csvPeriod)
{
    $period = New-Object -TypeName PeriodData;
    $period.CurveDay = $csvPeriod.CurveDay;
    $period.PeriodNumber = $csvPeriod.PeriodNumber;
    $period.StartPeriodTime = $csvPeriod.StartPeriodTime;
    $period.FinishPeriodTime = $csvPeriod.FinishPeriodTime;
    $period.MinTemperature = $csvPeriod.MinTemperature;
    $period.MaxTemperature = $csvPeriod.MaxTemperature;
    $period;
}

$dane=import-csv -delimiter ';' $curveFile | Convert-CsvToPeriod | Where-Object -FilterScript {$curveId.Days -eq $_.CurveDay}
$dane|Set-WitControllerSetting03 -Id $Id -TemperatureCurve

