﻿#Skrypt demonstrujący użycie cmdletu Set-WitControllerTemperatureCurve do ustawienia krzywej temperatury 
#odczytanej z podanego pliku csv. Z powodu braku możliwości przekazania danych przez potok, musimy
#wywołać cmdlet w pętli foreach 

Param
(
	[string] $Id, #identyfikator kontrolera
    [DateTime] $startCurveDate, #data rozpoczecia krzywej
    [string] $curveFile # nazwa pliku z krzywą
)
Clear-Host;
$curveId=(Get-Date).Date - $startCurveDate;
Add-Type @"
public struct PeriodData
{
    public int CurveDay;
    public int PeriodNumber;
    public string StartPeriodTime;
    public string FinishPeriodTime;
    public double MinTemperature;
    public double MaxTemperature;
}
"@
Filter Convert-CsvToPeriod([Parameter(Mandatory = $true, ValueFromPipeline = $true)]$csvPeriod)
{
    $period = New-Object -TypeName PeriodData;
    $period.CurveDay = $csvPeriod.CurveDay;
    $period.PeriodNumber = $csvPeriod.PeriodNumber;
    $period.StartPeriodTime = $csvPeriod.StartPeriodTime;
    $period.FinishPeriodTime = $csvPeriod.FinishPeriodTime;
    $period.MinTemperature = $csvPeriod.MinTemperature;
    $period.MaxTemperature = $csvPeriod.MaxTemperature;
    $period;
}
$dane=import-csv -delimiter ';' $curveFile | Convert-CsvToPeriod | Where-Object -FilterScript {$curveId.Days -eq $_.CurveDay}
foreach($d in $dane)
{
    Set-WitControllerTemperatureCurve -Id $Id -Period ($d.PeriodNumber, $d.StartPeriodTime, $d.FinishPeriodTime, $d.MinTemperature, $d.MaxTemperature)
}
