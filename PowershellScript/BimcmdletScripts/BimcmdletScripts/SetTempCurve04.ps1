﻿#Przykład wprowadzenia danych z potoku i obliczenia temperatru przez skrypt. Pozostałe parametry podawane są przez nazwę.
Param
(
    [string] $Id,
    [DateTime] $startCurveDate,
    [double] $baseTemperature,
    [string] $curveFile
)
Clear-Host;
$curveId=(Get-Date).Date - $startCurveDate;
Add-Type @"
public struct PeriodDeviatio
{
    public int CurveDay;
    public int PeriodNumber;
    public string StartPeriodTime;
    public string FinishPeriodTime;
    public double MinDeviatio;
    public double MaxDeviatio;
}
"@
Filter Convert-CsvToPeriod([Parameter(Mandatory = $true, ValueFromPipeline = $true)]$csvPeriod)
{
    $period = New-Object -TypeName PeriodDeviatio;
    $period.CurveDay = $csvPeriod.CurveDay;
    $period.PeriodNumber = $csvPeriod.PeriodNumber;
    $period.StartPeriodTime = $csvPeriod.StartPeriodTime;
    $period.FinishPeriodTime = $csvPeriod.FinishPeriodTime;
    $period.MinDeviatio = $csvPeriod.MinDeviatio;
    $period.MaxDeviatio = $csvPeriod.MaxDeviatio;
    $period;
}
$dane=import-csv -delimiter ';' $curveFile | Convert-CsvToPeriod | Where-Object -FilterScript {$curveId.Days -eq $_.CurveDay}
$dane|Set-WitControllerSetting03  -Id $Id -TemperatureCurve -MinTemperature {$baseTemperature - $_.MinDeviatio} -MaxTemperature {$baseTemperature + $_.MaxDeviatio}

