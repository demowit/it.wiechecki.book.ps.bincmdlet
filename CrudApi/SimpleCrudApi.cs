﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;


namespace Wiechecki.CrudApi
{
    public class SimpleCrudApi
    {
        #region private basic http methods
        public static async Task<string> Post(string requestUri, string data)
        {
            HttpClient client = new HttpClient();
            StringContent content = new StringContent(data);
            HttpResponseMessage response = await client.PostAsync(requestUri, content);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> Delete(string requestUri)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.DeleteAsync(requestUri);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> Put(string requestUri, string data)
        {
            HttpClient client = new HttpClient();
            StringContent content = new StringContent(data);
            HttpResponseMessage response = await client.PutAsync(requestUri, content);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> Get(string requestUri)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(requestUri);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }
        #endregion
        #region API
        private string baseUri = string.Empty;
        public SimpleCrudApi(string baseUri)
        {
            this.baseUri = baseUri;
        }
        /// <summary>
        /// Creates at target objects from data
        /// </summary>
        /// <typeparam name="T1">Type of Web Api response</typeparam>
        /// <typeparam name="T2">Type of processed data</typeparam>
        /// <param name="baseUri">Web Api base Uri</param>
        /// <param name="target">Name of processed target</param>
        /// <param name="data">Processed data. It can contains single object or objects collection. For correct processing
        /// WebApi is responisble.</param>
        /// <returns>WebApi response object</returns>
        public T1 Create<T1, T2>(string target, T2 data)
        {
            string result = Post(string.Format("{0}/{1}", baseUri.Trim('/'), target.Trim('/')), JsonConvert.SerializeObject(data)).Result;
            return JsonConvert.DeserializeObject<T1>(result);
        }
        /// <summary>
        /// Deletes object with given id from target.
        /// </summary>
        /// <typeparam name="T">Type of WebApi return response.</typeparam>
        /// <param name="baseUri">Web Api base uri</param>
        /// <param name="target">Name of target</param>
        /// <param name="id">Id of object</param>
        /// <returns>WebApi response object</returns>
        public T Delete<T>(string target)
        {
            string result = Delete(string.Format("{0}/{1}", baseUri.Trim('/'), target.Trim('/'))).Result;
            return JsonConvert.DeserializeObject<T>(result);
        }
        /// <summary>
        /// Update record with given id at target.
        /// </summary>
        /// <typeparam name="T1">Type of WebApi response</typeparam>
        /// <typeparam name="T2">Type of processed data</typeparam>
        /// <param name="baseUri">Web api uri</param>
        /// <param name="target">Name of processed target</param>
        /// <param name="id">Id of processed record</param>
        /// <param name="data">Data to process</param>
        /// <returns>WebApi response object</returns>
        public T1 Update<T1, T2>(string target, T2 data)
        {
            string result = Put(string.Format("{0}/{1}", baseUri.Trim('/'), target.Trim('/')), JsonConvert.SerializeObject(data)).Result;
            return JsonConvert.DeserializeObject<T1>(result);
        }
        /// <summary>
        /// Return record with given id 
        /// </summary>
        /// <typeparam name="T">Type of object with respnse from WebAPI</typeparam>
        /// <param name="baseUri">Web api uri</param>
        /// <param name="target">Name of procesed target</param>
        /// <param name="id">Id of processed record</param>
        /// <returns>WebApi response object</returns>
        public T Read<T>(string target)
        {
            string result = Get(string.Format("{0}/{1}", baseUri.TrimEnd('/'), target.Trim('/'))).Result;
            return JsonConvert.DeserializeObject<T>(result);
        }
        #endregion
    }
}
