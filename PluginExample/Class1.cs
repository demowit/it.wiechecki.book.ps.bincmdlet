﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;


namespace PluginExample
{
    public class Cricle
    {
        public static Object GetArea(PSObject instance)
        {
            double d = Convert.ToDouble(instance.Properties["Radius"].Value);
            return (d * 2 * 3.14);
        }

        public static void SetArea(PSObject instance, Object value)
        {
            instance.Properties["Radius"].Value = Convert.ToDouble(value) / 2 / 3.14;
        }
    }

    public class Sphere
    {
        public static Object GetArea(PSObject instance)
        {
            double d = Convert.ToDouble(instance.Properties["Radius"].Value);
            return (d * d * 4 * 3.14);
        }

        public static void SetArea(PSObject instance, Object value)
        {
            instance.Properties["Radius"].Value = Math.Sqrt(Convert.ToDouble(value) / (4 * 3.14));
        }
    }

}
