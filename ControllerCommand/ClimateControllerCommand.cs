﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Reflection;
using Microsoft.Win32;


namespace Wiechecki.ClimateController
{
    public partial class PluginMessage
    {
        public string VerboseMessage = string.Empty;
        public string DebugMessage = string.Empty;
    }

    public interface IControllerCommand
    {
        string GetPluginName();
        void SetControllerRegistryKeyName(string keyName = null);
        ControllerData[] GetData(string ctrlAddress, Action<PluginMessage> action = null);
        ControllerSettings[] GetSetting(string ctrlAddress, Action<PluginMessage> action = null);
        void UpdateSettings(string ctrlAddress, ControllerSettings controllerSettings, Action<PluginMessage> action = null);
        ControllerState[] GetState(string ctrlAddress, Action<PluginMessage> action = null);
        Period[] GetCurve(string ctrlAddress, int? periodId = null, Action<PluginMessage> action = null);
        void UpdatePeriod(string ctrlAddress, Period period, Action<PluginMessage> action = null);
        void DeletePeriod(string ctrlAddress, int periodId, Action<PluginMessage> action = null);
    }

    public class DummyPluginMessage : PluginMessage
    {
        public DummyPluginMessage(string methodName = "")
        {
            this.VerboseMessage = (methodName.Length == 0) ? string.Format("You use dummy plugin.") : string.Format("Method: {0}.You use dummy plugin.", methodName);
            this.DebugMessage = (methodName.Length == 0) ? string.Format("You use dummy plugin.") : string.Format("Method: {0}.You use dummy plugin.", methodName);
        }
    }
    public class DummyPlugin : IControllerCommand
    {
        public static string PluginName = "dummy";
        public static string controllerRegistryKeyName = null;
        public string GetPluginName()
        {
            return PluginName;
        }
        public void SetControllerRegistryKeyName(string keyName = null)
        {
            controllerRegistryKeyName = keyName;
        }
        public ControllerData[] GetData(string ctrlAddress, Action<PluginMessage> action = null)
        {
            action?.Invoke(new DummyPluginMessage("GetData"));
            return new ControllerData[0];
        }
        public ControllerSettings[] GetSetting(string ctrlAddress, Action<PluginMessage> action = null)
        {
            action?.Invoke(new DummyPluginMessage("GetSetting"));
            return new ControllerSettings[0];
        }
        public void UpdateSettings(string ctrlAddress, ControllerSettings controllerSettings, Action<PluginMessage> action = null)
        {
            action?.Invoke(new DummyPluginMessage("UpdateSetting"));
        }
        public ControllerState[] GetState(string ctrlAddress, Action<PluginMessage> action = null)
        {
            action?.Invoke(new DummyPluginMessage("GetState"));
            return new ControllerState[0];
        }
        public Period[] GetCurve(string ctrlAddress, int? periodId = null, Action<PluginMessage> action = null)
        {
            action?.Invoke(new DummyPluginMessage("GetCurve"));
            return new Period[0];
        }
        public void UpdatePeriod(string ctrlAddress, Period period, Action<PluginMessage> action = null)
        {
            action?.Invoke(new DummyPluginMessage("UpdatePeriod"));
        }
        public void DeletePeriod(string ctrlAddress, int periodId, Action<PluginMessage> action = null)
        {
            action?.Invoke(new DummyPluginMessage("DeletePeriod"));
        }

    }
    public class ClimateControllerCommand
    {
        #region Dane i metody odpowiedzialne za lokalną bazę danych
        private static int maxControllerCount = 5;
        private static string mainRegistryKeyName = @"software\wiechecki.it\cmdlet book";

        public string GetMainRegistryKey()
        {
            return mainRegistryKeyName;
        }

        public static void RemoveData()
        {
            try
            {
                Registry.CurrentUser.DeleteSubKey(mainRegistryKeyName);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static void NewDatabase()
        {
            ControllerInformation[] result = new ControllerInformation[maxControllerCount];
            for (int i = 0; i < maxControllerCount; i++)
            {
                result[i] = new ControllerInformation(string.Format("c0{0}", i + 1));
            }
            SaveDatabase(result);
        }
        private static string databaseValueName = "database";
        private static void SaveDatabase(ControllerInformation[] database)
        {            
            string result = JsonConvert.SerializeObject(database);
            RegistryKey regKey = Registry.CurrentUser.CreateSubKey(mainRegistryKeyName);
            regKey.SetValue(databaseValueName, result);
            regKey.Close();
        }

        private static ControllerInformation[] GetDatabase()
        {
            RegistryKey regKey = Registry.CurrentUser.CreateSubKey(mainRegistryKeyName);
            NewDatabase();
            string database = regKey.GetValue(databaseValueName).ToString();
            regKey.Close();
            return JsonConvert.DeserializeObject<ControllerInformation[]>(database);
        }
        private static ControllerInformation FindController(ControllerInformation[] database, string id)
        {
            foreach (ControllerInformation ci in database)
            {
                if ((ci.Id.Equals(id.ToLower())) && (ci.Available))
                {
                    return ci;
                }
            }
            return null;
        }
        public static ControllerInformation NewController(string id, string name, string[] location, int serialNumber)
        {
            ControllerInformation[] database = GetDatabase();
            ControllerInformation ci = FindController(database, id);
            if (ci != null)
            {
                ci.Name = name;
                ci.ManagedLocation = location;
                ci.SerialNumber = serialNumber;
                ci.Available = false;
                SaveDatabase(database);
                
            }
            return ci;
        }
        public static ControllerInformation NewController(string id, string name, string location, int serialNumber)
        {
            return NewController(id, name, new string[1] { location }, serialNumber);
        }
        public static ControllerInformation SetController(string id, string name, string[] location, int? serialNumber)
        {
            ControllerInformation[] database = GetDatabase();
            ControllerInformation ci = FindController(database, id);
            if (ci != null)
            {
                if (name != null)
                {
                    ci.Name = name;
                }
                if (location != null)
                {
                    ci.ManagedLocation = location;
                }
                if (serialNumber != null)
                {
                    ci.SerialNumber = (int)serialNumber;
                }
                SaveDatabase(database);                
            }
            return ci;
        }
        public static ControllerInformation SetController(string id, string name, string location, int? serialNumber)
        {
            return SetController(id, name, new string[1] { location }, serialNumber);
        }

        public static ControllerInformation GetController(string id, bool useId)
        {
            ControllerInformation[] database = GetDatabase();
            foreach (ControllerInformation ci in database)
            {
                if (useId)
                {
                    if (ci.Id.Equals(id.ToLower()))
                    {
                        return ci;
                    }
                }
                else
                {
                    if (ci.Name.ToLower().Equals(id.ToLower()))
                    {
                        return ci;
                    }
                }
            }
            return null;
        }

        public static ControllerInformation[] GetController()
        {
            ControllerInformation[] database = GetDatabase();
            int count = 0;
            foreach (ControllerInformation ci in database)
            {
                if (!ci.Available)
                {
                    ++count;
                }
            }
            ControllerInformation[] result = new ControllerInformation[count];
            int i = 0;
            foreach (ControllerInformation ci in database)
            {
                if (!ci.Available)
                {
                    result[i] = ci;
                    ++i;
                }
            }
            return result;
        }

        public static ControllerInformation RemoveController(string id)
        {
            ControllerInformation[] database = GetDatabase();
            foreach (ControllerInformation ci in database)
            {
                if (ci.Id.Equals(id))
                {
                    ci.Available = true;
                    SaveDatabase(database);
                    return ci;
                }
            }
            return null;
        }
        public static ControllerInformation[] RemoveController()
        {
            ControllerInformation[] database = GetDatabase();
            foreach (ControllerInformation item in database)
            {
                item.Available = true;
            }
            SaveDatabase(database);
            return database;
        }
        #endregion
        #region Dane i metody odpowiedzialne za komunikację z kontrolerami
        /// Komunikacja z kontrolerami może odbywać się przez Internet lub po RS485
        /// W obu przypadkach stosujemy interfejs IRemoteControllerCommand odczytany z biblioteki
        /// której nazwa jest zapisana w rejestrze, w wartości PluginNameValueName
        private const string PluginNameValueName = "PluginName";
        private static PluginLoader plugins = null;
        private static PluginLoader Plugins(bool force = false)
        {
            if ((force) || (plugins == null))
            {
                plugins = new PluginLoader(Environment.CurrentDirectory, "GetPluginName");
            }
            return plugins;
        }

        private static IControllerCommand ControllerCommand
        {
            get
            {
                string pluginName = DummyPlugin.PluginName;
                RegistryKey regKey = Registry.CurrentUser.CreateSubKey(mainRegistryKeyName);                    
                pluginName = regKey.GetValue(PluginNameValueName, DummyPlugin.PluginName).ToString();
                regKey.Close();                    
                Plugins()[pluginName].SetControllerRegistryKeyName(mainRegistryKeyName);
                return Plugins()[pluginName];
            }
        }
        public static string[] GetPlugin(bool force)
        {
            return Plugins(force).Keys.ToArray();
        }
        public static bool SetPlugin(string pluginName)
        {
            try
            {
                bool result = Plugins().ContainsKey(pluginName);
                if (result)
                {
                    RegistryKey regKey = Registry.CurrentUser.CreateSubKey(mainRegistryKeyName, RegistryKeyPermissionCheck.ReadWriteSubTree);
                    regKey.SetValue(PluginNameValueName, pluginName);
                    regKey.Close();
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static ControllerData[] GetData(string ctrlAddress, Action<PluginMessage> action = null)
        {            
            try
            {
                return ControllerCommand.GetData(ctrlAddress, action);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static ControllerSettings[] GetSetting(string ctrlAddress, Action<PluginMessage> action = null)
        {
            try
            {
                return ControllerCommand.GetSetting(ctrlAddress, action);
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        public static void UpdateSettings(string ctrlAddress, ControllerSettings controllerSettings, Action<PluginMessage> action = null)
        {
            try
            {
                ControllerCommand.UpdateSettings(ctrlAddress, controllerSettings, action);
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        public static ControllerState[] GetState(string ctrlAddress, Action<PluginMessage> action = null)
        {
            try
            {
                return ControllerCommand.GetState(ctrlAddress, action);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static Period[] GetCurve(string ctrlAddress, int? periodId = null, Action<PluginMessage> action = null)
        {
            try
            {
                return ControllerCommand.GetCurve(ctrlAddress, periodId, action);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public static void UpdatePeriod(string ctrlAddress, Period period, Action<PluginMessage> action = null)
        {
            try
            {
                ControllerCommand.UpdatePeriod(ctrlAddress, period, action);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static void DeletePeriod(string ctrlAddress, int periodId, Action<PluginMessage> action = null)
        {
            try
            {
                ControllerCommand.DeletePeriod(ctrlAddress, periodId, action);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}
