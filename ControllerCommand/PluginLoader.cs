﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace Wiechecki.ClimateController
{
    public sealed class PluginLoader : Dictionary<string, IControllerCommand>
    {
        private string getPluginNameMethod = string.Empty;
        public PluginLoader(string path, string getPluginNameMethod)
        {
            this.getPluginNameMethod = getPluginNameMethod;
            Load(path);
        }
        private void Load(string path)
        {
            if (Directory.Exists(path))
            {
                //ICollection<Assembly> assemblies = new List<Assembly>(dllNames.Length);
                Type pluginType = typeof(IControllerCommand);
                foreach (string dllFile in Directory.GetFiles(path, "*.dll"))
                {
                    AssemblyName assemblyName = AssemblyName.GetAssemblyName(dllFile);
                    Assembly assembly = Assembly.Load(assemblyName);
                    //assemblies.Add(assembly);
                   
                    if (assembly != null)
                    {
                        foreach(Type type in assembly.GetTypes())
                        {
                            if ((!type.IsAbstract) && (!type.IsInterface) && (type.GetInterface(pluginType.FullName) != null))
                            {
                                IControllerCommand plugin = (IControllerCommand)Activator.CreateInstance(type);
                                MethodInfo methodInfo = type.GetMethod(getPluginNameMethod);
                                if (methodInfo != null)
                                {
                                    string pluginName = (string)methodInfo.Invoke(plugin, null);
                                    this.Add(pluginName, plugin);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
