﻿using System;
using System.Globalization;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Management.Automation;

namespace Wiechecki.ClimateController
{
    public class ControllerInformation
    {
        public string Name { get; set; } = string.Empty;
        public string[] ManagedLocation { get; set; }
        public int SerialNumber { get; set; } = 0;
        public string Id { get; } = string.Empty;
        public bool Available { get; set; } = true;
        public ControllerInformation(string id)
        {
            this.Id = id;
        }
    }

    

    public class ControllerData
    {
        public string CtrlId { get; set; }
        public int ReadoutId { get; set; }
        public DateTime ReadoutTimeStamp { get; set; }
        public double? Temperature { get; set; } = 0;
        public double? Humidity { get; set; } = 0;
    }

    public class ControllerSettings
    {
        public string CtrlId { get; set; }
        public double? MinTemperature { get; set; } = null;
        public double? MaxTemperature { get; set; } = null;
        public double? MinHumidity { get; set; } = null;
        public double? MaxHumidity { get; set; } = null;
    }

    public class ControllerState
    {
        public string CtrlId { get; set; }
        public int AirConditionerState { get; set; } = 0;
        public int HeatingState { get; set; } = 0;
        public int HumidificationState { get; set; } = 0;
    }

    public class Period
    {
        public int PeriodId { get; set; }
        public int PeriodAvailable { get; set; }
        public TimeSpan? StartPeriodTime { get; set; }
        public TimeSpan? FinishPeriodTime { get; set; }
        public Double? MinTemperature { get; set; }
        public Double? MaxTemperature { get; set; }
    }


    public class ControllerTemperatureConfiguration
    {
        /// <summary>
        /// Jeśli TemperatureCurve = true to:
        /// StartperiodTime, FinishPeriodTime, PeriodNumber muszą być ustawione.
        /// null oznacza, że parametru nie zmieniamy
        /// </summary>
        public SwitchParameter TemperatureCurve
        {
            get;
            set;
        }
        public Double? MinTemperature { get; set; }
        public Double? MaxTemperature { get; set; }
        public int PeriodNumber { get; set; }
        public SwitchParameter ResetPeriod { get; set; }
        public TimeSpan? StartPeriodTime { get; set; }
        public TimeSpan? FinishPeriodTime { get; set; }


        public ControllerTemperatureConfiguration(SwitchParameter temperatureCurve)
        {
            this.TemperatureCurve = temperatureCurve;
        }

        public ControllerTemperatureConfiguration(int periodNumber, TimeSpan? startPeriodTime, TimeSpan? finishPeriodTime, Double? minTemperature, Double? maxTemperature)
        {
            this.TemperatureCurve = true;            
            this.PeriodNumber = periodNumber;
            this.MinTemperature = minTemperature;
            this.MaxTemperature = maxTemperature;
            this.StartPeriodTime = startPeriodTime;
            this.FinishPeriodTime = finishPeriodTime;
        }
        public ControllerTemperatureConfiguration(int periodNumber)
        {
            this.TemperatureCurve = true;
            this.ResetPeriod = true;
            this.PeriodNumber = periodNumber;
        }
    }

    public class ControllerHumidityConfiguration
    {
        public Double? MinHumidity { get; set; } = null;
        public Double? MaxHumidity { get; set; } = null;
    }

}
