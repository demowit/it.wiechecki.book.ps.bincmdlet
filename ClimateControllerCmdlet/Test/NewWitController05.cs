﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja parametrów obowiązkowych i pozycyjnych
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.New, "WitController05")]
    public class NewWITController05 : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        public string Id { get; set; }
        [Parameter(Mandatory = true, Position = 1)]
        public string Name { get; set; }
        [Parameter()]
        public string[] ManagedLocation { get; set; }
        [Parameter(Position = 2)]
        public int SerialNumber { get; set; }
        protected override void ProcessRecord()
        {
            ControllerInformation ci = ClimateControllerCommand.NewController(Id, Name, ManagedLocation, SerialNumber);
            WriteObject(ci);
        }
    }
}
