﻿using System.Collections.Generic;
using System.Management.Automation;
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitController01")]
    public class GetWITController01 : Cmdlet
    {
        protected override void ProcessRecord()
        {
            List<WitController> list = new List<WitController>();
            list.Add(new WitController("c01", "Hala A1", "Kowalski M"));
            list.Add(new WitController("c02", "Hala A2", "Malinowski A"));
            list.Add(new WitController("c03", "Hala B", "Konopa G"));
            list.Add(new WitController("c04", "Hala C", "Michalak K"));
            list.Add(new WitController("c05", "Hala D", "Maćkowiak L"));
            foreach(WitController item in list)
            {
                WriteObject(item);
            }
        }
    }
}
