﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Set, "WitControllerTemperatureCurve01")]
    public class SetWitControllerTemperatureCurve01 : Cmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter(ParameterSetName = "Reset period")]
        public SwitchParameter ResetPeriod { get; set; }
        [Parameter(Mandatory = true, ParameterSetName ="Set period")]
        public int PeriodNumber { get; set; }
        [Parameter(ParameterSetName = "Set period")]
        public Double? MinTemperature { get; set; }
        [Parameter(ParameterSetName = "Set period")]
        public Double? MaxTemperature { get; set; }
        [Parameter(ParameterSetName = "Set period")]
        public TimeSpan? StartPeriodTime { get; set; }
        [Parameter(ParameterSetName = "Set period")]
        public TimeSpan? FinishPeriodTime { get; set; }
        private ControllerTemperatureConfiguration PeriodTemperature
        {
            get
            {
                ControllerTemperatureConfiguration result = new ControllerTemperatureConfiguration(true)
                {
                    ResetPeriod = ResetPeriod,
                    PeriodNumber = PeriodNumber,
                    MinTemperature = MinTemperature,
                    MaxTemperature = MaxTemperature,
                    StartPeriodTime = StartPeriodTime,
                    FinishPeriodTime = FinishPeriodTime
                };
                return result;
            }
        }
        protected override void ProcessRecord()
        {
            //WriteObject(RemoteControllerCommand.SetSettings(Id, PeriodTemperature, null));
        }

    }
}
