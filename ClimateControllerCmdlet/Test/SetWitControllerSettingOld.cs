﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;


namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    public class ScriptedGlobalTemperatureConfiguration
    {
        [Parameter()]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        [ScriptBlock()]
        public Object MinTemperature { get; set; }
        [Parameter()]
        [ScriptBlock()]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Object MaxTemperature { get; set; }
    }

    public class ScriptedTemperaturePeriodConfiguration
    {
        [Parameter(Mandatory = true)]
        [ValidateRangeNotScript(ValidationData.MinPeriodId, ValidationData.MaxPeriodId)]
        [ScriptBlock()]
        public Object PeriodNumber { get; set; }
        [Parameter()]
        [ValidateRangeNotScript(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        [ScriptBlock()]
        public Object StartPeriodTime { get; set; }
        [Parameter()]
        [ValidateRangeNotScript(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        [ScriptBlock()]
        public Object FinishPeriodTime { get; set; }
        [Parameter()]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        [ScriptBlock()]
        public Object MinTemperature { get; set; }
        [Parameter()]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        [ScriptBlock()]
        public Object MaxTemperature { get; set; }
    }

    public class ScriptedResetTemperaturePeriod
    {
        [Parameter(Mandatory = true)]
        [ValidateRangeNotScript(ValidationData.MinPeriodId, ValidationData.MaxPeriodId)]
        [ScriptBlock()]
        public Object PeriodNumber { get; set; }
    }


    [Cmdlet(VerbsCommon.Set, "WitControllerSettingOld")]
    public class SetWitControllerSettingOld : Cmdlet, IDynamicParameters
    {
        [Parameter(ValueFromPipeline = true)]
        public Object InputObject { get; set; }
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        [ValidateRangeNotScript(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        [ScriptBlock()]
        public Object MinHumidity { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        [ValidateRangeNotScript(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        [ScriptBlock()]
        public Object MaxHumidity { get; set; }
        /// <summary>
        /// SetTemperatureCurve = true to ustawione muszą być:
        /// PeriodNumber, StartPeriodTime, FinishPeriodTime, SetMinTemperature, SetMaxTemperature, MinTemperature, MaxTemperature.
        /// Jeśli SetTemperatureCurve = false (domyślnie) to mogą być ustawione
        /// SetMinTemperature, MinTemperature, SetMaxTemperature, MaxTemperature. 
        /// </summary>
        [Parameter()]
        public SwitchParameter TemperatureCurve { get; set; }
        private SwitchParameter resetPeriod;
        [Parameter()]
        public SwitchParameter ResetPeriod
        {
            get
            {
                return this.resetPeriod;
            }
            set
            {
                this.resetPeriod = value;
                if (this.resetPeriod)
                {
                    this.TemperatureCurve = true;
                }
            }
        }
        [Parameter()]
        private ScriptedGlobalTemperatureConfiguration globalTemperatureConfiguration = null;
        [Parameter()]
        private ScriptedTemperaturePeriodConfiguration temperaturePeriodConfiguration = null;
        /// <summary>
        /// <para type="description">To jest parametr dynamiczny</para>
        /// </summary>
        [Parameter()]
        private ScriptedResetTemperaturePeriod resetTemperaturePeriod = null;
        public Object GetDynamicParameters()
        {
            if (TemperatureCurve && (!ResetPeriod))
            {
                temperaturePeriodConfiguration = new ScriptedTemperaturePeriodConfiguration();
                return temperaturePeriodConfiguration;
            };
            if (TemperatureCurve && ResetPeriod)
            {
                resetTemperaturePeriod = new ScriptedResetTemperaturePeriod();
                return resetTemperaturePeriod;
            };
            if ((!TemperatureCurve) && (!ResetPeriod))
            {
                globalTemperatureConfiguration = new ScriptedGlobalTemperatureConfiguration();
                return globalTemperatureConfiguration;
            };
            return null;
        }
        private ControllerHumidityConfiguration HumidityConfiguration
        {
            get
            {
                ControllerHumidityConfiguration result = new ControllerHumidityConfiguration();
                result.MinHumidity = ParameterConverter.ToDouble(MinHumidity, InputObject);
                result.MaxHumidity = ParameterConverter.ToDouble(MaxHumidity, InputObject);
                return result;
            }
        }
        private ControllerTemperatureConfiguration TemperatureConfiguration
        {
            get
            {
                ControllerTemperatureConfiguration result = null;
                if (globalTemperatureConfiguration != null)
                {
                    result = new ControllerTemperatureConfiguration(false);
                    result.MaxTemperature = ParameterConverter.ToDouble(globalTemperatureConfiguration.MinTemperature, InputObject);
                    result.MinTemperature = ParameterConverter.ToDouble(globalTemperatureConfiguration.MaxTemperature, InputObject);
                }
                if (temperaturePeriodConfiguration != null)
                {
                    result = new ControllerTemperatureConfiguration(true);
                    result.MaxTemperature = ParameterConverter.ToDouble(temperaturePeriodConfiguration.MinTemperature, InputObject);
                    result.MinTemperature = ParameterConverter.ToDouble(temperaturePeriodConfiguration.MaxTemperature, InputObject);
                    result.PeriodNumber = ParameterConverter.ToInt(temperaturePeriodConfiguration.PeriodNumber, InputObject);
                    result.StartPeriodTime = ParameterConverter.ToTimeSpan(temperaturePeriodConfiguration.StartPeriodTime, InputObject);
                    result.FinishPeriodTime = ParameterConverter.ToTimeSpan(temperaturePeriodConfiguration.FinishPeriodTime, InputObject);
                }
                if (resetTemperaturePeriod != null)
                {
                    result = new ControllerTemperatureConfiguration(true);
                    result.PeriodNumber = ParameterConverter.ToInt(resetTemperaturePeriod.PeriodNumber, InputObject);
                    result.ResetPeriod = true;
                }
                return result;
            }
        }

        protected override void ProcessRecord()
        {
            //WriteObject(RemoteControllerCommand.SetSettings(
            //    Id,
            //    TemperatureConfiguration,
            //    HumidityConfiguration
            //    )
            //    );
        }
    }
}
