﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja zbiorów parametrów
/// DefaultParameterSet
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitController05", DefaultParameterSetName = "All controllers")]
    public class GetWITController05 : Cmdlet
    {
        [Parameter(Mandatory = true, ParameterSetName = "All controllers")]
        public SwitchParameter All { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Controllers by name")]
        public string[] Name { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Controllers by InternalId")]
        public string[] Id { get; set; }
        protected override void ProcessRecord()
        {
            ControllerInformation[] controllers = ClimateControllerCommand.GetController();
            if (All)
            {
                WriteObject(controllers, true);
            }
            if (Name != null)
            {
                foreach(string name in Name)
                {
                    ControllerInformation ci = ClimateControllerCommand.GetController(name, false);
                    if (ci != null)
                    {
                        WriteObject(ci);
                    }
                }
            }
            if (Id != null)
            {
                foreach (string id in Id)
                {
                    ControllerInformation ci = ClimateControllerCommand.GetController(id, true);
                    if (ci != null)
                    {
                        WriteObject(ci);
                    }
                }
            }
        }
    }
}
