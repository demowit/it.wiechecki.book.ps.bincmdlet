﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;


#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    public class ValidateTemperaturePeriodAttribute : ValidateArgumentsAttribute
    {
        protected override void Validate(object arguments, EngineIntrinsics engineIntrinsics)
        {
           if(arguments is ControllerTemperatureConfiguration[])
            {
                ControllerTemperatureConfiguration[] ctccol = arguments as ControllerTemperatureConfiguration[];
                foreach (ControllerTemperatureConfiguration ctc in ctccol)
                {
                    if (!((ctc.PeriodNumber >= this.minPeriodNumber) && (ctc.PeriodNumber <= this.maxPeriodNumber)))
                    {
                        throw new Exception("Period number out of range");
                    }
                    if(!((ctc.MinTemperature != null) && (ctc.MinTemperature >= this.minTemperature) && (ctc.MinTemperature <= this.maxTemperature)))
                    {
                        throw new Exception("Minimum temperature out of range");
                    }
                    if (!((ctc.MaxTemperature != null) && (ctc.MaxTemperature >= this.minTemperature) && (ctc.MaxTemperature <= this.maxTemperature)))
                    {
                        throw new Exception("Maximum temperature out of range");
                    }
                    if ((ctc.MinTemperature != null) && (ctc.MaxTemperature != null) && (ctc.MinTemperature >= ctc.MaxTemperature))
                    {
                        throw new Exception("Minimum temperature greater then maximum temperature");
                    }
                }
            }
        }
        private int minPeriodNumber;
        private int maxPeriodNumber;
        private Double minTemperature;
        private Double maxTemperature;
        public ValidateTemperaturePeriodAttribute(int minPeriodNumber, int maxPeriodNumber, Double minTemperature, Double maxTemperature)
        {
            this.minPeriodNumber = minPeriodNumber;
            this.maxPeriodNumber = maxPeriodNumber;
            this.minTemperature = minTemperature;
            this.maxTemperature = maxTemperature;
        }
    }

    [Cmdlet(VerbsCommon.Set, "WitControllerTemperatureCurve04")]
    public class SetWitControllerTemperatureCurve04 : Cmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Set period collection")]
        [ListToTemperatureConfigurationConverter()]
        [ValidateTemperaturePeriod(1, 24, -30.00, 70.00)]
        public ControllerTemperatureConfiguration[] Period { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Reset period")]        
        public int[] PeriodNumber { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Reset period")]
        public SwitchParameter ResetPeriod { get; set; }
        /// <summary>
        /// Z powodów demosntracyjnych metoda ProcessRecord() nie dokonuje działań na kontrolerach, jedynie wyświetla otrzymane dane.
        /// </summary>
        protected override void ProcessRecord()
        {
            if (ResetPeriod)
            {
                foreach (int periodNumber in PeriodNumber)
                {
                    ControllerTemperatureConfiguration temperatureConfiguration;
                    temperatureConfiguration = new ControllerTemperatureConfiguration(periodNumber);
                }
                return;
            }
            if (Period != null)
            {
                WriteObject(Period, true);
            }
        }

    }
}
