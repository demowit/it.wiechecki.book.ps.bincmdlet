﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;


/// <summary>
/// Implementacja danych z potoku. Dane wejściowe jako właściwość obiektu. 
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitControllerData03")]
    public class GetWITControllerData03 : Cmdlet
    {
        [Parameter(ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        protected override void ProcessRecord()
        {
            ControllerData[] cd = ClimateControllerCommand.GetData(Id);
            WriteObject(cd, true);
        }
    }
}
