﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja danych z potoku. Dane wejściowe jako właściwość obiektu. 
/// </summary>
#pragma warning disable 1591

namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitControllerData04")]
    public class GetWITControllerData04 : Cmdlet
    {
        [Parameter(ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        public string[] ManagedLocation { get; set; }
        protected override void ProcessRecord()
        {
            WriteObject(ManagedLocation);
            ControllerData[] cd = ClimateControllerCommand.GetData(Id);            
            WriteObject(cd, true);
        }
    }
}
