﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Remove, "WitController03", SupportsShouldProcess = true, ConfirmImpact = ConfirmImpact.High)]
    public class RemoveWitController03 : PSCmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        [ValidatePattern(ValidationData.ControllerIdPattern)]
        public string Id { get; set; }
        private bool yesToAll;
        private bool noToAll;
        [Parameter()]
        public SwitchParameter Force { get; set; }
        protected override void ProcessRecord()
        {
            if (Id != null)
            {
                if ((Force) || (ShouldProcess(Id)))
                {
                    if ((Force) ||(ShouldContinue("Czy wykonać operację?", "Usunięcie kontrolera. Powtórne zapytanie.", ref yesToAll, ref noToAll)))
                    {
                        WriteObject(ClimateControllerCommand.RemoveController(Id));
                    }
                }
            }
        }
    }
}
