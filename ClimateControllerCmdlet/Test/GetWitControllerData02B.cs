﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja danych z potoku. Dane wejściowe jako kolekcja. Różnica między pobraniem danych z potoku a otrzymaniem kolekcji.
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitControllerData02B")]
    public class GetWITControllerData02B : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation[] InputObject;
        private static int counter = 1;
        protected override void BeginProcessing()
        {
            counter = 1;
        }
        protected override void ProcessRecord()
        {
            WriteObject("ProcessRecord().Start");
            WriteObject(string.Format("Typ obiektu {0}. Liczba elementów kolekcji {1}. Numer wywołania {2}", InputObject.GetType().ToString(), InputObject.Length, counter));
            for(int i = 0; i < InputObject.Length; i++)
            {
                WriteObject(string.Format("Element nr {0}", i));
                WriteObject(string.Format("Kontroler id {0}", InputObject[i].Id));
            }
            counter++;
            WriteObject("ProcessRecord().Finish" + Environment.NewLine);
        }
    }
}
