﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja danych z potoku. Dane wejściowe jako właściwość obiektu. 
/// Implementacja HashTable
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{

    public class ExtendedControllerData
    {
        public PSObject CustomObject;
        public ExtendedControllerData()
        {
            this.CustomObject = new PSObject();
        }

        public static Object ExecuteScript(ScriptBlock scriptBlock, Object processedObject)
        {
            string str = scriptBlock.ToString().Replace("$_", "$args[0]");
            ScriptBlock scr = ScriptBlock.Create(str);
            var scriptResult = scr.Invoke(processedObject)[0];
            return scr.Invoke(processedObject)[0];
        }

        public void AddProperty(string propertyName, Object propertyValue)
        {
            PSNoteProperty newProperty = new PSNoteProperty(propertyName, propertyValue);
            this.CustomObject.Properties.Add(newProperty);
        }

        public void AddProperty(Hashtable hashTable, Object processedObject)
        {
            string propertyName = string.Empty;
            ScriptBlock propertyValue = null;
            foreach (DictionaryEntry de in hashTable)
            {
                string script = de.Value.ToString();
                if (de.Key.Equals("name"))
                {
                    propertyName = de.Value.ToString();
                }
                if (de.Key.Equals("expression"))
                {
                    propertyValue = de.Value as ScriptBlock;
                }
            }
            AddProperty(propertyName, propertyValue, processedObject);
        }
        public void AddProperty(string propertyName, ScriptBlock scriptBlock, Object processedObject)
        {
            PSNoteProperty newProperty = new PSNoteProperty(propertyName, ExecuteScript(scriptBlock, processedObject));
            this.CustomObject.Properties.Add(newProperty);
        }
    }

    [Cmdlet(VerbsCommon.Get, "WitControllerData05")]
    public class GetWITControllerData05 : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        public string[] ManagedLocation { get; set; }
        [Parameter()]
        public Hashtable[] Property { get; set; }
        protected override void ProcessRecord()
        {
            ControllerData[] cd = ClimateControllerCommand.GetData(Id);
            ExtendedControllerData ecd = new ExtendedControllerData();
            ecd.AddProperty("ControllerData", cd);
            ecd.AddProperty("Id", Id);
            PSObject psobj = new PSObject();
            PSNoteProperty newProperty = new PSNoteProperty("ControllerID", Id);
            psobj.Properties.Add(newProperty);
            newProperty = new PSNoteProperty("ControllerData", cd);
            psobj.Properties.Add(newProperty);
            WriteObject(psobj);
        }
    }
}
