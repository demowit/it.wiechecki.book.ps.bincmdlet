﻿using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Przykład implementacji parameterów domyślnych
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.New, "WitController02")]
    public class NewWITController02 : Cmdlet
    {
        [Parameter()]
        public string Id { get; set; }
        [Parameter()]
        public string Name { get; set; }
        [Parameter()]
        public string ManagedLocation { get; set; } = "Hala B";
        [Parameter()]
        public int SerialNumber { get; set; }
        protected override void ProcessRecord()
        {
            ControllerInformation ci = ClimateControllerCommand.NewController(Id, Name, ManagedLocation, SerialNumber);
            WriteObject(ci);
        }
    }
}
