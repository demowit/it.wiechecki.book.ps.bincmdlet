﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja parametrów obowiązkowych
/// </summary>
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.New, "WitController03")]
    public class NewWITController03 : Cmdlet
    {
        [Parameter(Mandatory = true)]
        public string Id { get; set; }
        [Parameter(Mandatory = true)]
        public string Name { get; set; }
        [Parameter()]
        public string ManagedLocation { get; set; }
        [Parameter()]
        public int SerialNumber;
        protected override void ProcessRecord()
        {
            ControllerInformation ci = ClimateControllerCommand.NewController(Id, Name, ManagedLocation, SerialNumber);
            WriteObject(ci);
        }
    }
}
