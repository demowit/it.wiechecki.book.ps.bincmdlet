﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "Error02")]
    public class GetError02 : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public string Id { get; set; }
        protected override void ProcessRecord()
        {
            try
            {
                throw new Exception("Błąd podczas przetwarzania danych");
            }
            catch (Exception e)
            {
                ErrorRecord errorRecord = new ErrorRecord(e, "ErrorId01", ErrorCategory.InvalidData, Id);
                errorRecord.ErrorDetails = new ErrorDetails("Błąd testowy - szczegółowy opis");                
                WriteError(errorRecord);
            }

        }
    }
}
