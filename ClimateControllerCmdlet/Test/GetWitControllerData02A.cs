﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja danych z potoku. Dane wejściowe jako kolekcja. Wyświetlanie jako InputObject[0]!!!
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitControllerData02A")]
    public class GetWitControllerData02A : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject
        {
            get;
            set;
        }
        private static int counter = 1;
        protected override void BeginProcessing()
        {
            counter = 1;
        }
        protected override void ProcessRecord()
        {
            WriteObject("ProcessRecord().Start");
            WriteObject(string.Format("Typ obiektu: {0}. Numer wywołania: {1}", InputObject.GetType().ToString(), counter));
            WriteObject(string.Format("Kontroler id {0}", InputObject.Id));
            WriteObject("ProcessRecord().Finish" + Environment.NewLine);
            counter++;
        }
    }
}
