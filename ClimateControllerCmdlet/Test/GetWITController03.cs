﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitController03")]
    public class GetWITController03 : Cmdlet
    {
        protected override void ProcessRecord()
        {
            List<WitController> list = new List<WitController>()
            {
                new WitController("c01", "Hala A1", "Kowalski M"),
                new WitController("c02", "Hala A2", "Malinowski A"),
                new WitController("c03", "Hala B", "Konopa G"),
                new WitController("c04", "Hala C", "Michalak K"),
                new WitController("c05", "Hala D", "Maćkowiak L"),
            };
            WriteObject(list, true);
        }
    }
}
