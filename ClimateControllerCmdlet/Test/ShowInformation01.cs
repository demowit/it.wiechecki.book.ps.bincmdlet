﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Show, "Information01")]
    public class ShowInformation01 : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject { get; set; }
        [Parameter()]
        public int Max { get; set; } = 10;
        protected override void ProcessRecord()
        {
            WriteVerbose(string.Format("Sprawdzamy id obiektu {0}", InputObject.Id));
            WriteDebug(string.Format("Id: {0} SN: {1}", InputObject.Id, InputObject.SerialNumber));
            if (InputObject.Id.Equals("c01"))
            {
                WriteWarning("Uwaga. Przetwarzamy obiekt c01");
            }
            WriteObject(InputObject);            
        }
    }
}
