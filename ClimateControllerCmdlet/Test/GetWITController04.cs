﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Wprowadzenie do zbiorów parametrów. Przykład pokazujący problem ze standardowym podejściem do podawanych parametrów
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitController04")]
    public class GetWITController04 : Cmdlet
    {
        [Parameter()]
        public SwitchParameter All { get; set; }
        [Parameter()]
        public string[] Name { get; set; }
        [Parameter()]
        public string[] Id { get; set; }
        protected override void ProcessRecord()
        {
            ControllerInformation[] controllers = ClimateControllerCommand.GetController();
            if (All)
            {
                WriteObject(controllers, true);
            }
            if (Name != null)
            {
                foreach (string name in Name)
                {
                    WriteObject(ClimateControllerCommand.GetController(name, false));
                }
            }
            if (Id != null)
            {
                foreach(string id in Id)
                {
                    WriteObject(ClimateControllerCommand.GetController(id, true));
                }
            }
        }
    }
}
