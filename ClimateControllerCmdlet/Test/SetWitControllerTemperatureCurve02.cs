﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Set, "WitControllerTemperatureCurve02")]
    public class SetWitControllerTemperatureCurve02 : Cmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Set period")]
        public ControllerTemperatureConfiguration[] Period { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Reset period")]
        public int[] PeriodId { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Reset period")]
        public SwitchParameter ResetPeriod { get; set; }
        protected override void ProcessRecord()
        {
            if (ResetPeriod)
            {
                foreach(int periodId in PeriodId)
                {
                    //WriteObject(RemoteControllerCommand.SetSettings(Id, new ControllerTemperatureConfiguration(periodId), null));
                }
                return;              
            }
            foreach(ControllerTemperatureConfiguration period in Period)
            {
                //WriteObject(RemoteControllerCommand.SetSettings(Id, period, null));
            }            
        }

    }
}
