﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591

namespace Wiechecki.ClimateControllerCmdlet.Examples
{

    [Cmdlet(VerbsCommon.Show, "Progress01", SupportsShouldProcess = true, ConfirmImpact = ConfirmImpact.Medium)]
    public class ShowProgress01 : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject { get; set; }
        [Parameter()]
        public int Max { get; set; } = 50;
        protected override void ProcessRecord()
        {

            ProgressRecord pr = new ProgressRecord(100, "Pętla główna", "start");
            ProgressRecord pr1 = new ProgressRecord(101, "Podpętla", "start");
            WriteProgress(pr);
            if (ShouldProcess("Kontynuować?", "Uwaga"))
            {
                for (int i = 0; i < Max; i++)
                {
                    pr.PercentComplete = i * 100 / Max;
                    pr.SecondsRemaining = Max - i;
                    pr.StatusDescription = string.Format("Status pr description {0}", i);
                    pr.CurrentOperation = string.Format("Current pr operation {0}", i);
                    pr.RecordType = ProgressRecordType.Processing;
                    WriteProgress(pr);
                    for (int j = 0; j < Max; j++)
                    {
                        pr1.PercentComplete = j * 100 / Max;
                        pr1.SecondsRemaining = Max - j;
                        pr1.StatusDescription = string.Format("Status pr 1description {0}", j);
                        pr1.CurrentOperation = string.Format("Current pr1 operation {0}", j);
                        pr1.RecordType = ProgressRecordType.Processing;
                        WriteProgress(pr1);
                    }
                }
            }
            pr.StatusDescription = string.Format("Koniec");
            WriteProgress(pr);
        }
    }
}
