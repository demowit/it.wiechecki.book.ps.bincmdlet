﻿using System;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitControllerData06")]
    public class GetWitControllerData06 : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        [ValidatePattern(ValidationData.ControllerIdPattern)]
        public string Id { get; set; }
        [Parameter()]
        public SwitchParameter ETS { get; set; }
        [Parameter()]
        [ScriptBlock()]
        public ScriptBlock Script { get; set; }
        [Parameter()]
        public Hashtable[] UserProperty { get; set; }
        [Parameter()]
        public Hashtable[] UserScriptMethod { get; set; }
        [Parameter()]
        public Hashtable[] UserCodeMethod { get; set; }
        private Object GetKeyValue(string fullKeyName, DictionaryEntry dictionaryEntry, object inputObject)
        {
            return inputObject ?? ((fullKeyName.IndexOf(dictionaryEntry.Key.ToString().ToLower()) == 0) ? dictionaryEntry.Value : null);
        }

        private PSObject MakeObject(ControllerData controllerData)
        {
            PSObject obj = new PSObject(controllerData);
            if (InputObject != null)
            {
                obj.Properties.Add(new PSNoteProperty("ControllerName", InputObject.Name));
                obj.Properties.Add(new PSNoteProperty("Location", InputObject.ManagedLocation));
                obj.Properties.Add(new PSNoteProperty("SerialNumber", InputObject.SerialNumber));
            }
            obj.Properties.Add(new PSNoteProperty("DownloadTime", DateTime.Now));
            obj.Properties.Add(new PSAliasProperty("LastReadoutTime", "ReadoutTimeStamp"));
            obj.Properties.Add(new PSAliasProperty("CurrentTemperature", "Temperature"));
            obj.Properties.Add(new PSAliasProperty("CurrentHumidity", "Humidity"));
            obj.Properties.Add(new PSScriptProperty("LastAccessReadoutTimeDiff", ScriptBlock.Create("[DateTime]::Now - $this.LastReadoutTime")));
            return obj;
         }
        private void AddScript(ref PSObject obj)
        {
            if(Script != null)
            {
                obj.Properties.Add(new PSNoteProperty("TemperatureDeviation", Script.Invoke(InputObject)[0]));
            }
        }
        private void AddUserProperty(ref PSObject obj)
        {
            if (UserProperty != null)
            {
                foreach (Hashtable hs in UserProperty)
                {
                    Object propertyName = null;
                    Object propertyValue = null;
                    foreach (DictionaryEntry de in hs)
                    {
                        propertyName = GetKeyValue("name", de, propertyName);
                        propertyValue = GetKeyValue("expression", de, propertyValue);
                    }
                    if ((propertyName != null) && (propertyValue != null))
                    {
                        obj.Properties.Add(new PSScriptProperty(propertyName.ToString(), propertyValue as ScriptBlock));
                    }
                }
            }
        }
        private void AddUserScriptMethod(ref PSObject obj)
        {
            if (UserScriptMethod != null)
            {
                foreach (Hashtable hs in UserScriptMethod)
                {
                    Object propertyName = null;
                    Object propertyValue = null;
                    foreach (DictionaryEntry de in hs)
                    {
                        propertyName = GetKeyValue("name", de, propertyName);
                        propertyValue = GetKeyValue("expression", de, propertyValue);
                    }
                    if ((propertyName != null) && (propertyValue != null))
                    {
                        obj.Methods.Add(
                            new PSScriptMethod(
                                propertyName.ToString(), 
                                propertyValue as ScriptBlock
                                ));
                    }
                }
            }
        }
        private void AddUserCodeMethod(ref PSObject obj)
        {
            if (UserCodeMethod != null)
            {
                foreach (Hashtable hs in UserCodeMethod)
                {
                    Object pluginName = null;
                    Object methodName = null;
                    Object targetMethodName = null;
                    foreach (DictionaryEntry de in hs)
                    {
                        pluginName = GetKeyValue("pluginname", de, pluginName);
                        methodName = GetKeyValue("sourcemethodname", de, methodName);
                        targetMethodName = GetKeyValue("targetmethodname", de, targetMethodName);
                    }
                    if ((pluginName != null) && (methodName != null) && (targetMethodName != null))
                    {
                        string path = Environment.CurrentDirectory + "\\Plugins\\" + pluginName.ToString();
                        Assembly asm = Assembly.LoadFile(path);
                        Type[] tpc = asm.GetTypes();
                        if (tpc != null)
                        {
                            foreach (Type tp in tpc)
                            {
                                MethodInfo[] mic = tp.GetMethods();
                                foreach(MethodInfo mi in mic)
                                {
                                    string mname = tp.Name + "." + mi.Name;
                                    if (mname.Equals(methodName.ToString()))
                                    {
                                        obj.Methods.Add(new PSCodeMethod(targetMethodName.ToString(), mi));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        protected override void ProcessRecord()
        {
            ControllerData[] cdc = ClimateControllerCommand.GetData(Id);
            if (ETS)
            {
                foreach(ControllerData cd in cdc)
                {
                    PSObject obj = MakeObject(cd);
                    AddScript(ref obj);
                    AddUserProperty(ref obj);
                    AddUserScriptMethod(ref obj);
                    AddUserCodeMethod(ref obj);
                    WriteObject(obj);
                }
            }
            else
            {
                WriteObject(cdc, true);
            }
        }
    }
}
