﻿using System;
using System.Globalization;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    public class ScriptBlockAttribute : ArgumentTransformationAttribute
    {
        protected ScriptBlock RemoveAlias(ScriptBlock scriptBlock)
        {
            string str = scriptBlock.ToString().Replace("$_", "$args[0]");
            str = Regex.Replace(str, "[$][P|p][S|s][I|i][T|t][E|e][M|m]", "$args[0]");
            return ScriptBlock.Create(str);
        }

        public override object Transform(EngineIntrinsics engineIntrinsics, object inputData)
        {
            object input = inputData;
            if (input is PSObject)
            {
                input = ((PSObject)input).BaseObject;
            }
            if (input is ScriptBlock)
            {
                return RemoveAlias(input as ScriptBlock);
            }
            return inputData;
        }
    }

    public class ValidateRangeNotScriptAttribute : ValidateArgumentsAttribute
    {
        private void CheckDoubleData(Object data)
        {
            if ((Convert.ToDouble(data) < Convert.ToDouble(this.min)) || (Convert.ToDouble(data) > Convert.ToDouble(this.max)))
            {
                throw new Exception("Argument out of range");
            }
        }

        private void ValidateData(object item)
        {
            if ((item == null) || (item is ScriptBlock) || (item is Hashtable))
            {
                return;
            }

            if ((item != null) && (item is int))
            {
                if ((this.min is int) || (this.max is int) || (this.min is double) || (this.max is double) || (this.min is float) || (this.max is float) || (this.min is decimal) || (this.max is decimal))
                {
                    CheckDoubleData(item);
                }
                return;
            }
            if ((item != null) && (item is PSObject))
            {
                if ((((PSObject)item).BaseObject != null) && ((((PSObject)item).BaseObject is Int64) || (((PSObject)item).BaseObject is UInt64) || (((PSObject)item).BaseObject is double) || (((PSObject)item).BaseObject is float) || (((PSObject)item).BaseObject is decimal)))
                {
                    Object data = ((PSObject)item).BaseObject;
                    CheckDoubleData(data);
                }
                return;
            }
            if ((item != null) && (item is string))
            {
                Match match = Regex.Match(item as string, "^[-]?[1-9]{1}[0-9]*[.]?[0-9]*$");//sprawdzamy czy wprowadzono liczbę ujemną z częścią dziesiętną
                if (match.Success)
                {
                    item = (item as string).Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
                    CheckDoubleData(item);
                    return;
                }
                if (!((string.Compare((string)this.min, (string)item, true) < 0) && (string.Compare((string)this.max, (string)item, true) > 0)))
                {
                    throw new Exception("Data out of range");
                }
                return;
            }
            return;
        }
        protected override void Validate(object arguments, EngineIntrinsics engineIntrinsics)
        {
            if (!this.min.GetType().Equals(this.max.GetType()))
            {
                throw new ValidationMetadataException();
            }
            if (arguments is Object[])
            {
                Object[] objcolection = arguments as Object[];
                foreach (Object obj in objcolection)
                {
                    ValidateData(obj);
                }
            }
            else
            {
                ValidateData(arguments);
            }
        }
        private Object min;
        private Object max;
        public ValidateRangeNotScriptAttribute(Object min, Object max)
        {
            this.min = min;
            this.max = max;
        }
    }    

    public class ScriptedGlobalTemperatureConfiguration
    {
        [Parameter()]
        [ScriptBlock()]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Object MinTemperature { get; set; }
        [Parameter()]
        [ScriptBlock()]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Object MaxTemperature { get; set; }
    }

    public class ScriptedTemperaturePeriodConfiguration
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        [ValidateRangeNotScript(ValidationData.MinPeriodId, ValidationData.MaxPeriodId)]
        [ScriptBlock()]
        public Object PeriodNumber { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        [ValidateRangeNotScript(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        [ScriptBlock()]
        public Object StartPeriodTime { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        [ValidateRangeNotScript(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        [ScriptBlock()]
        public Object FinishPeriodTime { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        [ScriptBlock()]
        public Object MinTemperature
        {
            get;
            set;
        }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        [ValidateRangeNotScript(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        [ScriptBlock()]
        public Object MaxTemperature { get; set; }
    }

    public class ScriptedResetTemperaturePeriod
    {
        [Parameter(Mandatory = true)]
        [ValidateRangeNotScript(ValidationData.MinPeriodId, ValidationData.MaxPeriodId)]
        [ScriptBlock()]
        public Object PeriodNumber { get; set; }
    }

    [Cmdlet(VerbsCommon.Set, "WitControllerSetting03")]
    public class SetWitControllerSetting03 : Cmdlet, IDynamicParameters
    {
        [Parameter(ValueFromPipeline = true)]
        public Object InputObject { get; set; }
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter()]
        [ValidateRangeNotScript(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        [ScriptBlock()]
        public Object MinHumidity { get; set; }
        [Parameter()]
        [ValidateRangeNotScript(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        [ScriptBlock()]
        public Object MaxHumidity { get; set; }
        [Parameter()]
        public SwitchParameter TemperatureCurve { get; set; }
        private SwitchParameter resetPeriod;
        [Parameter()]
        public SwitchParameter ResetPeriod
        {
            get
            {
                return this.resetPeriod;
            }
            set
            {
                this.resetPeriod = value;
                if (this.resetPeriod)
                {
                    this.TemperatureCurve = true;
                }
            }
        }
        private ScriptedGlobalTemperatureConfiguration globalTemperatureConfiguration = null;
        private ScriptedTemperaturePeriodConfiguration temperaturePeriodConfiguration = null;
        private ScriptedResetTemperaturePeriod resetTemperaturePeriod = null;
        public Object GetDynamicParameters()
        {
            if (TemperatureCurve && (!ResetPeriod))
            {
                temperaturePeriodConfiguration = new ScriptedTemperaturePeriodConfiguration();
                return temperaturePeriodConfiguration;
            };
            if (TemperatureCurve && ResetPeriod)
            {
                resetTemperaturePeriod = new ScriptedResetTemperaturePeriod();
                return resetTemperaturePeriod;
            };
            if ((!TemperatureCurve) && (!ResetPeriod))
            {
                globalTemperatureConfiguration = new ScriptedGlobalTemperatureConfiguration();
                return globalTemperatureConfiguration;
            };
            return null;
        }
        private Double? ConvertToDouble(Object inputData)
        {
            if ((inputData != null) && (inputData is ScriptBlock))
            {
                PSObject ps = (inputData as ScriptBlock).Invoke(InputObject)[0];
                return Convert.ToDouble(ps.BaseObject, new CultureInfo("en-US"));
            }
            if (inputData != null)
            {
                Object obj = inputData;
                if(inputData is PSObject)
                {
                    obj = (inputData as PSObject).BaseObject;
                }
                return Convert.ToDouble(obj, new CultureInfo("en-US"));
            }
            return null;
        }
        private TimeSpan? ConvertToTimeSpan(Object inputData)
        {
            if((inputData != null) && (inputData is ScriptBlock))
            {
                PSObject ps = (inputData as ScriptBlock).Invoke(InputObject)[0];
                TimeSpan.TryParse(ps.BaseObject.ToString(), out TimeSpan ts);
                return ts;
            }
            if(inputData != null)
            {
                TimeSpan.TryParse(inputData.ToString(), out TimeSpan ts);
                return ts;
            }
            return null;
        }
        private int ConvertToInt(Object inputData)
        {
            if ((inputData != null) && (inputData is ScriptBlock))
            {
                PSObject ps = (inputData as ScriptBlock).Invoke(InputObject)[0];
                return Convert.ToInt32(ps.BaseObject);
            }
            if (inputData != null)
            {                
                return Convert.ToInt32(inputData);
            }
            return 0;
        }
        private ControllerHumidityConfiguration HumidityConfiguration
        {
            get
            {
                ControllerHumidityConfiguration result = new ControllerHumidityConfiguration()
                {
                    MinHumidity = ConvertToDouble(MinHumidity),
                    MaxHumidity = ConvertToDouble(MaxHumidity),
                };
                return result;
            }
        }
        private ControllerTemperatureConfiguration TemperatureConfiguration
        {
            get
            {
                ControllerTemperatureConfiguration result = null;
                if (globalTemperatureConfiguration != null)
                {
                    result = new ControllerTemperatureConfiguration(false)
                    {
                        MaxTemperature = ConvertToDouble(globalTemperatureConfiguration.MaxTemperature),
                        MinTemperature = ConvertToDouble(globalTemperatureConfiguration.MinTemperature)
                    };
                }
                if (temperaturePeriodConfiguration != null)
                {
                    result = new ControllerTemperatureConfiguration(true)
                    {
                        MaxTemperature = ConvertToDouble(temperaturePeriodConfiguration.MaxTemperature),
                        MinTemperature = ConvertToDouble(temperaturePeriodConfiguration.MinTemperature),
                        PeriodNumber = ConvertToInt(temperaturePeriodConfiguration.PeriodNumber),
                        StartPeriodTime = ConvertToTimeSpan(temperaturePeriodConfiguration.StartPeriodTime),
                        FinishPeriodTime = ConvertToTimeSpan(temperaturePeriodConfiguration.FinishPeriodTime)
                    };
                }
                if (resetTemperaturePeriod != null)
                {
                    result = new ControllerTemperatureConfiguration(true)
                    {
                        PeriodNumber = ConvertToInt(resetTemperaturePeriod.PeriodNumber),
                        ResetPeriod = true
                    };
                }
                return result;
            }
        }

        protected override void ProcessRecord()
        {
            WriteObject(TemperatureConfiguration);
            WriteObject(HumidityConfiguration);
        }
    }
}
