﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja danych z potoku. Dane jako pojedyńczy element.
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "WitControllerData01")]
    public class GetWITControllerData01 : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject { get; set; }
        protected override void ProcessRecord()
        {
            ControllerData[] cd = ClimateControllerCommand.GetData(InputObject.Id);
            WriteObject(cd, true);
        }
    }
}
