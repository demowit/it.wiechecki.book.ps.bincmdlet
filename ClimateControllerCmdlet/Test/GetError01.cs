﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Get, "Error01")]    
    public class GetError01 : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject { get; set; }
        protected override void ProcessRecord()
        {
            try
            {
                if ((InputObject.Id.Equals("c01")) || (InputObject.Id.Equals("c03")))
                {
                    throw new Exception("Błąd podczas przetwarzania danych");
                }
                WriteObject(InputObject);
            }
            catch (Exception e)
            {
                ErrorRecord errorRecord = new ErrorRecord(e, "ErrorId01", ErrorCategory.InvalidData, InputObject);
                WriteError(errorRecord);
            }
            
        }
    }
}
