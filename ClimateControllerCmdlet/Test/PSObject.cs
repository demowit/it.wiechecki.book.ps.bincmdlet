﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;
using System.Reflection;

namespace thistest
{
    public class CodePropertyPlugin
    {
        public static double MyPluginFunGet(PSObject instance)
        {
            return (Double)instance.Properties["Humidity"].Value / 2;
        }
        public static void MyPluginFunSet(PSObject instance, double input)
        {
            instance.Properties["Humidity"].Value = input * 2;
        }
    }
}

namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsDiagnostic.Test, "CustomObject")]
    public class CustomObject : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public ControllerInformation InputObject { get; set; }
        [Parameter(ValueFromPipelineByPropertyName = true)]
        public string Id { get; set;}
        [Parameter()]
        public SwitchParameter ExternalVar { get; set; }
        protected override void ProcessRecord()
        {
            if (ExternalVar)
            {
                PSObject obj = new PSObject();
                //obj.Properties.Add(new PSVariableProperty(new PSVariable("Var1", 10, ScopedItemOptions.AllScope)));
                obj.Properties.Add(new PSNoteProperty("Radius", 5));
                obj.Properties.Add(new PSScriptProperty("Area", ScriptBlock.Create("$this.Radius*2*3.14"), ScriptBlock.Create("param([double]$val);$this.radius=$val/2/3.14")));
                //obj.Properties["prop"].Value = 2;
                WriteObject(obj);
                return;
            }
            ControllerData[] cdc = ClimateControllerCommand.GetData(Id);
            if (InputObject != null)
            {
                foreach(ControllerData cd in cdc)
                {
                    PSObject obj = new PSObject(cd);
                    PSAliasProperty ap = new PSAliasProperty("CurrentTemperature", "Temperature", typeof(string));
                    obj.Properties.Add(ap);
                    ap = new PSAliasProperty("CurrentHumidity", "Humidity");
                    obj.Properties.Add(ap);
                    PSNoteProperty np = new PSNoteProperty("Id", InputObject.Id);
                    obj.Properties.Add(np);
                    np = new PSNoteProperty("BaseTemperature", 15.00);
                    obj.Properties.Add(np);
                    //PSVariable psvar = new PSVariable("ExternalVar");
                    //PSVariableProperty vp = new PSVariableProperty(psvar);
                    //obj.Properties.Add(vp);
                    ScriptBlock sb = ScriptBlock.Create("$ExternalVar - $this.BaseTemperature -$this.CurrentTemperature");
                    PSScriptProperty sp = new PSScriptProperty("Deviatio", sb);
                    obj.Properties.Add(sp);
                    MethodInfo getter = typeof(thistest.CodePropertyPlugin).GetMethod("MyPluginFunGet");
                    MethodInfo setter = typeof(thistest.CodePropertyPlugin).GetMethod("MyPluginFunSet");
                    PSCodeProperty cp = new PSCodeProperty("Averragehumidity", getter, setter);
                    obj.Properties.Add(cp);
                    WriteObject(obj);
                }
            }
        }
    }
}
