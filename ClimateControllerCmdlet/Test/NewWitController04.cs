﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;


/// <summary>
/// Implementacja parametry obowiązkowe i pomoc dla nich.
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.New, "WitController04")]
    public class NewWITController04 : Cmdlet
    {
        [Parameter(Mandatory = true, HelpMessage = "Identyfikator sieciowy kontrolera. Podaj c01, c02, c03, c04, c05")]
        public string Id { get; set; }
        [Parameter(Mandatory = true, HelpMessage = "Podaj nazwę kontrolera")]
        public string Name { get; set; }
        [Parameter()]
        public string ManagedLocation { get; set; }
        [Parameter()]
        public int SerialNumber;
        protected override void ProcessRecord()
        {
            ControllerInformation ci = ClimateControllerCommand.NewController(Id, Name, ManagedLocation, SerialNumber);
            WriteObject(ci);
        }
    }
}
