﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    /// <summary>
    /// Chuj
    /// </summary>
    [Cmdlet(VerbsCommon.Show, "PSCmdlet", SupportsShouldProcess = true, ConfirmImpact = ConfirmImpact.High)]
    public class ShowPSCmdlet : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true, HelpMessage = "To są dane wejściowe")]
        public ControllerInformation InputObject { get; set; }
        [Parameter(HelpMessage = "To jest jakiś niewykorzystany parametr")]
        public SwitchParameter ParamOne { get; set; }
        protected override void ProcessRecord()
        {
            if (ShouldProcess("Czy kontynuować?"))
            {
                WriteObject(string.Format("Max object {0}.Current object {1}", this.MyInvocation.PipelineLength, this.MyInvocation.PipelinePosition));
            }
            
        }
    }
}
