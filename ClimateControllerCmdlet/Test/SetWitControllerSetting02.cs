﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    public class GlobalTemperatureConfiguration
    {
        [Parameter()]
        [ValidateRange(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Double? MinTemperature { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Double? MaxTemperature { get; set; }
    }

    public class TemperaturePeriodConfiguration
    {
        [Parameter(Mandatory = true)]
        [ValidateRange(ValidationData.MinPeriodId, ValidationData.MaxPeriodId)]
        public int PeriodNumber { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        public TimeSpan? StartPeriodTime { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        public TimeSpan? FinishPeriodTime { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Double? MinTemperature { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Double? MaxTemperature { get; set; }
    }

    public class ResetTemperaturePeriod
    {
        [Parameter(Mandatory = true)]
        [ValidateRange(ValidationData.MinPeriodId, ValidationData.MaxPeriodId)]
        public int PeriodNumber { get; set; }
    }


    [Cmdlet(VerbsCommon.Set, "WitControllerSetting02")]
    public class SetWitControllerSetting02 : Cmdlet, IDynamicParameters
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        public Double? MinHumidity { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        public Double? MaxHumidity { get; set; }
        /// <summary>
        /// SetTemperatureCurve = true to ustawione muszą być:
        /// PeriodNumber, StartPeriodTime, FinishPeriodTime, SetMinTemperature, SetMaxTemperature, MinTemperature, MaxTemperature.
        /// Jeśli SetTemperatureCurve = false (domyślnie) to mogą być ustawione
        /// SetMinTemperature, MinTemperature, SetMaxTemperature, MaxTemperature. 
        /// </summary>
        [Parameter()]
        public SwitchParameter TemperatureCurve { get; set; }
        private SwitchParameter resetPeriod;
        [Parameter()]
        public SwitchParameter ResetPeriod
        {
            get
            {
                return this.resetPeriod;
            }
            set
            {
                this.resetPeriod = value;
                if (this.resetPeriod)
                {
                    this.TemperatureCurve = true;
                }
            }
        }
        private GlobalTemperatureConfiguration globalTemperatureConfiguration = null;
        private TemperaturePeriodConfiguration temperaturePeriodConfiguration = null;
        private ResetTemperaturePeriod resetTemperaturePeriod = null;
        public Object GetDynamicParameters()
        {
            if (TemperatureCurve && (!ResetPeriod))
            {
                temperaturePeriodConfiguration = new TemperaturePeriodConfiguration();
                return temperaturePeriodConfiguration;
            };
            if (TemperatureCurve && ResetPeriod)
            {
                resetTemperaturePeriod = new ResetTemperaturePeriod();
                return resetTemperaturePeriod;
            };
            if ((!TemperatureCurve) && (!ResetPeriod))
            {
                globalTemperatureConfiguration = new GlobalTemperatureConfiguration();
                return globalTemperatureConfiguration;
            };
            return null;
        }
        private ControllerHumidityConfiguration HumidityConfiguration
        {
            get
            {
                ControllerHumidityConfiguration result = new ControllerHumidityConfiguration()
                {
                    MaxHumidity = MaxHumidity,
                    MinHumidity = MinHumidity,
                };
                return result;
            }
        }
        private ControllerTemperatureConfiguration TemperatureConfiguration
        {
            get
            {
                ControllerTemperatureConfiguration result = null;
                if (globalTemperatureConfiguration != null)
                {
                    result = new ControllerTemperatureConfiguration(false)
                    {
                        MaxTemperature = globalTemperatureConfiguration.MaxTemperature,
                        MinTemperature = globalTemperatureConfiguration.MinTemperature
                    };
                }
                if (temperaturePeriodConfiguration != null)
                {
                    result = new ControllerTemperatureConfiguration(true)
                    {
                        MaxTemperature = temperaturePeriodConfiguration.MaxTemperature,
                        MinTemperature = temperaturePeriodConfiguration.MinTemperature,
                        PeriodNumber = temperaturePeriodConfiguration.PeriodNumber,
                        StartPeriodTime = temperaturePeriodConfiguration.StartPeriodTime,
                        FinishPeriodTime = temperaturePeriodConfiguration.FinishPeriodTime
                    };
                }
                if (resetTemperaturePeriod != null)
                {
                    result = new ControllerTemperatureConfiguration(true)
                    {
                        PeriodNumber = resetTemperaturePeriod.PeriodNumber,
                        ResetPeriod = true
                    };
                }
                return result;
            }
        }

        protected override void ProcessRecord()
        {
            WriteObject(TemperatureConfiguration);
            WriteObject(HumidityConfiguration);
            //WriteObject(RemoteControllerCommand.SetSettings(
            //    Id,
            //    TemperatureConfiguration,
            //    HumidityConfiguration
            //    )
            //    );
        }
    }
}
