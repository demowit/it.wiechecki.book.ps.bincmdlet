﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.Set, "WitControllerSetting01")]
    public class SetWitControllerSetting01 : Cmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        public Double? MinHumidity { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinHumidity, ValidationData.MaxHumidity)]
        public double? MaxHumidity { get; set; }
        /// <summary>
        /// SetTemperatureCurve = true to ustawione muszą być:
        /// PeriodNumber, StartPeriodTime, FinishPeriodTime, SetMinTemperature, SetMaxTemperature, MinTemperature, MaxTemperature.
        /// Jeśli SetTemperatureCurve = false (domyślnie) to mogą być ustawione
        /// SetMinTemperature, MinTemperature, SetMaxTemperature, MaxTemperature. 
        /// </summary>
        [Parameter()]
        public SwitchParameter TemperatureCurve { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinPeriodId, ValidationData.MaxPeriodId)]
        public int PeriodNumber { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        public TimeSpan StartPeriodTime { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.StartPeriodTime, ValidationData.FinishPeriodTime)]
        public TimeSpan FinishPeriodTime { get; set; }
        [Parameter()]
        public SwitchParameter SetMinTemperature { get; set; }
        [Parameter()]
        public SwitchParameter SetMaxTemperature { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Double MinTemperature { get; set; }
        [Parameter()]
        [ValidateRange(ValidationData.MinTemperature, ValidationData.MaxTemperature)]
        public Double MaxTemperature { get; set; }

        private ControllerHumidityConfiguration HumidityConfiguration
        {
            get
            {
                ControllerHumidityConfiguration result = new ControllerHumidityConfiguration()
                {
                    MaxHumidity = MaxHumidity,
                    MinHumidity = MinHumidity
                };
                return result;
            }
        }
        private ControllerTemperatureConfiguration TemperatureConfiguration
        {
            get
            {
                ControllerTemperatureConfiguration result = null;
                result = new ControllerTemperatureConfiguration(false)
                {
                    MaxTemperature = MaxTemperature,
                    MinTemperature = MinTemperature,
                    PeriodNumber = PeriodNumber,
                    StartPeriodTime = StartPeriodTime,
                    FinishPeriodTime = FinishPeriodTime,
                    TemperatureCurve = TemperatureCurve
                };
                return result;
            }
        }


        protected override void ProcessRecord()
        {
            //RemoteControllerCommand.SetSettings(
            //    Id,
            //    TemperatureConfiguration,
            //    HumidityConfiguration
            //    );
            //ControllerSettings cs = RemoteControllerCommand.GetSettings(Id);
            //WriteObject(cs);
            WriteObject(TemperatureConfiguration);
            WriteObject(HumidityConfiguration);
        }
    }
}
