﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{    
    [Cmdlet(VerbsCommon.Remove, "WitController01", SupportsShouldProcess = true, ConfirmImpact = ConfirmImpact.High)]
    public class RemoveWitController01 : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0, ValueFromPipelineByPropertyName = true)]
        [ValidatePattern(ValidationData.ControllerIdPattern)]
        public string Id { get; set; }
        protected override void ProcessRecord()
        {
            if (Id != null)
            {
                if (this.ShouldProcess("Opis gdy wywołamy z WhatIf", "Ostrzeżenie użytkownika", "Nagłówek ostrzeżenia", out ShouldProcessReason spr))
                {
                    
                    WriteObject(ClimateControllerCommand.RemoveController(Id));
                }
            }
        }
    }
}
