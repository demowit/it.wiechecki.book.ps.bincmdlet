﻿using System.Management.Automation;
using Wiechecki.ClimateController;

/// <summary>
/// Przykład implementacji parameterów
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.New, "WitController01")]
    public class NewWITController01 : Cmdlet
    {
        [Parameter()]
        public string Id { get; set; }
        [Parameter()]
        public string Name { get; set; }
        [Parameter()]
        public string ManagedLocation { get; set; }
        [Parameter()]
        public int SerialNumber { get; set; }
        protected override void ProcessRecord()
        {
            ControllerInformation ci = ClimateControllerCommand.NewController(Id, Name, ManagedLocation, SerialNumber);
            WriteObject(ci);
        }
    }
}
