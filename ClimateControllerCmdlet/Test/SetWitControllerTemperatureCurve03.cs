﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    public class TemperatureConfigurationConverterAttribute : ArgumentTransformationAttribute
    {
        protected TimeSpan? ToTimeSpan(Object input)
        {
            if (input == null)
            {
                return null;
            }
            TimeSpan.TryParse(input.ToString(), out TimeSpan ts);
            return ts;
        }
        protected Double? ToTemperature(Object input)
        {
            if (input == null)
            {
                return null;
            }
            return Convert.ToDouble(input.ToString());
        }
        private const int MaxParameterCount = 5;
        protected ControllerTemperatureConfiguration ConvertToControllerTemperatureConfiguration(Object item)
        {
            IList list = item as IList;
            if (list.Count == 0)
            {
                return null;
            }
            int periodNumber = 0;
            TimeSpan? startPeriodTime = null;
            TimeSpan? finishPeriodTime = null;
            Double? minTemperature = null;
            Double? maxTemperature = null;            
            for(int i = 0; (i < list.Count) && (i < MaxParameterCount); i++)
            {
                switch (i)
                {
                    case 0:
                        periodNumber = (int)list[i];
                        break;
                    case 1:
                        startPeriodTime = ToTimeSpan(list[i]);
                        break;
                    case 2:
                        finishPeriodTime = ToTimeSpan(list[i]);
                        break;
                    case 3:
                        minTemperature = ToTemperature(list[i]);
                        break;
                    case 4:
                        maxTemperature = ToTemperature(list[i]);
                        break;
                }
            }
            return new ControllerTemperatureConfiguration(periodNumber, startPeriodTime, finishPeriodTime, minTemperature, maxTemperature);
        }

        public override object Transform(EngineIntrinsics engineIntrinsics, object inputData)
        {
            object input = inputData;
            if (input is PSObject)
            {
                input = ((PSObject)input).BaseObject;
            }
            if (input is IList)
            {
                ControllerTemperatureConfiguration ctc = ConvertToControllerTemperatureConfiguration(input);
                if(ctc != null)
                {
                    return ctc;
                }
            }
            return inputData;
        }
    }
    public class ListToTemperatureConfigurationConverterAttribute : TemperatureConfigurationConverterAttribute
    {
        private ControllerTemperatureConfiguration[] OneElementCollection(Object input)
        {
            IList list = input as IList;
            ControllerTemperatureConfiguration[] ctccollection = null;
            if(list[0] is int)
            {
                ctccollection = new ControllerTemperatureConfiguration[1];
                ctccollection[0] = ConvertToControllerTemperatureConfiguration(input);
            }
            return ctccollection;
        }

        private ControllerTemperatureConfiguration[] ElementCollection(Object input)
        {
            IList list = input as IList;
            ControllerTemperatureConfiguration[] ctccollection = new ControllerTemperatureConfiguration[list.Count];
            int counter = 0;
            foreach (Object obj in list)
            {
                if ((obj is IList) || (obj is PSObject) || ((((PSObject)obj)).BaseObject is ControllerTemperatureConfiguration))
                {
                    if (obj is IList)
                    {
                        ctccollection[counter] = ConvertToControllerTemperatureConfiguration(obj);
                    }
                    if ((obj is PSObject) && ((((PSObject)obj)).BaseObject is ControllerTemperatureConfiguration))
                    {
                        ctccollection[counter] = (ControllerTemperatureConfiguration)((PSObject)obj).BaseObject;
                    }
                }
                counter++;
            }
            return ctccollection;
        }
        public override object Transform(EngineIntrinsics engineIntrinsics, object inputData)
        {
            object input = inputData;
            if (input is PSObject)
            {
                input = ((PSObject)input).BaseObject;
            }
            if (input is IList)
            {
                ControllerTemperatureConfiguration[] ctccollection = OneElementCollection(input);
                if(ctccollection == null)
                {
                    ctccollection = ElementCollection(input);
                }
                return ctccollection;
            }
            return inputData;
        }
    }

    [Cmdlet(VerbsCommon.Set, "WitControllerTemperatureCurve03")]
    public class SetWitControllerTemperatureCurve03 : Cmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Set period")]
        [TemperatureConfigurationConverter()]
        public ControllerTemperatureConfiguration SinglePeriod { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Set period collection")]
        [ListToTemperatureConfigurationConverter()]
        public ControllerTemperatureConfiguration[] Period { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Reset period")]
        public int[] PeriodNumber { get; set; }
        [Parameter(Mandatory = true, ParameterSetName = "Reset period")]
        public SwitchParameter ResetPeriod { get; set; }
        /// <summary>
        /// Z powodów demosntracyjnych metoda ProcessRecord() nie dokonuje działań na kontrolerach, jedynie wyświetla otrzymane dane.
        /// </summary>
        protected override void ProcessRecord()
        {            
            if (ResetPeriod)
            {
                foreach(int periodNumber in PeriodNumber)
                {
                    ControllerTemperatureConfiguration temperatureConfiguration;
                    temperatureConfiguration = new ControllerTemperatureConfiguration(periodNumber);
                    WriteObject(temperatureConfiguration);
                }
                return;
            }
            if (SinglePeriod != null)
            {
                WriteObject(SinglePeriod);
                return;
            }
            if (Period != null)
            {
                WriteObject(Period, true);
            }
        }

    }
}
