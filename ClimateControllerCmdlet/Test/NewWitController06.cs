﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using System.Text.RegularExpressions;
using Wiechecki.ClimateController;

/// <summary>
/// Implementacja walidacji danych
/// </summary>
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsCommon.New, "WitController06")]
    public class NewWITController06 : Cmdlet
    {
        [Parameter(Mandatory = true)]
        [ValidatePattern("^[C|c]0[1|2|3|4|5]$")]
        public string Id { get; set; }
        [Parameter(Mandatory = true, Position = 0)]
        [ValidatePattern("^[A-Z]{1}[a-z0-9 ]{1,19}$", Options = RegexOptions.Compiled)]
        public string Name { get; set; }
        [Parameter(Position = 1)]
        [ValidateCount(0, 2)]
        [ValidateLength(2, 12)]
        public string[] ManagedLocation { get; set; }
        [Parameter(Mandatory = true, Position = 2)]
        [ValidateRange(100, 200)]
        public int SerialNumber { get; set; }
        protected override void ProcessRecord()
        {
            ControllerInformation ci = ClimateControllerCommand.NewController(Id, Name, ManagedLocation, SerialNumber);
            WriteObject(ci);
        }
    }
}