﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    public class WitController
    {
        public string Name { get;}
        public string Description { get;}
        public string Administrator { get; }
        public WitController(string name, string description, string administrator)
        {
            this.Name = name;
            this.Description = description;
            this.Administrator = administrator;
        }
    }
}
