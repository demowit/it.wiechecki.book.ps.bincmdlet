﻿Projekt używa C#7 niedostępnego w VS 2013.
W VS 2015 należy zmienic kompilator zgodnie z intrukcjami:
http://stackoverflow.com/questions/39461407/how-to-use-c7-with-visual-studio-2015

md "$(ProjectDir)\Bin\Release"
xcopy "$(TargetPath)-Help.xml" "$(ProjectDir)\Bin\Release" /y
md "$(ProjectDir)\Bin\Debug"
xcopy "$(TargetPath)-Help.xml" "$(ProjectDir)\Bin\Debug" /y
del "$(ProjectDir)\Bin\Documentation\*.dll"
del "$(ProjectDir)\Bin\Documentation\*.pdb"
