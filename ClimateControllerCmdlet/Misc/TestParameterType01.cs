﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

#pragma warning disable 1591
namespace it.wiechecki.book.ps.bincmdlet.test
{
    [Cmdlet(VerbsDiagnostic.Test, "ParameterType01")]
    public class TestParameterType01 : Cmdlet
    {
        //Typy skalarne
        [Parameter()]
        public int IntParam { get; set; }
        public enum MyEnum { one, two }
        [Parameter()]
        public MyEnum MyEnumParam { get; set; }

        //nullable value types
        [Parameter()]
        public int? NullableIntParam { get; set; }
        [Parameter()]
        public int? DefaultNullableIntParam { get; set; } = 12;
        [Parameter()]

        public MyEnum? NullableMyEnumParam { get; set; }

        /// <summary>
        /// Typy referencyjne
        /// </summary>
        [Parameter()]
        public string StringParam { get; set; }
        [Parameter()]
        public string DefaultStringParam { get; set; } = "Wartość domyślna";
        [Parameter()]
        public PSCredential Credential {get; set; }
        [Parameter()]
        public PSCredential DefaultCredential { get; set; } = new PSCredential("user", new System.Security.SecureString());


        /// <summary>
        /// Różnica między bool a SwitchParameter
        /// </summary>
        [Parameter()]
        public Boolean BooleanParam { get; set; }
        [Parameter()]
        public SwitchParameter SwitchParam {get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(string.Format("Wartość IntParam: {0}", IntParam));
            WriteObject(string.Format("Wartość NullableIntParam: {0}. Jest null {1}", NullableIntParam, NullableIntParam == null));
            WriteObject(string.Format("Wartość DefaultNullableIntParam: {0}. Jest null {1}", DefaultNullableIntParam, DefaultNullableIntParam == null));
            WriteObject(string.Format("Wartość MyenumParam: {0}", MyEnumParam));
            WriteObject(string.Format("Wartość NullableMyEnumParam: {0}. Jest null {1}", NullableMyEnumParam, NullableMyEnumParam == null));
            WriteObject(string.Format("Wartość StringParam: {0}. Jest null {1}", StringParam, StringParam == null));
            WriteObject(string.Format("Wartość DefaultStringParam: {0}. Jest null {1}", DefaultStringParam, DefaultStringParam == null));
            WriteObject(string.Format("Wartość BooleanParam: {0}", BooleanParam));
            WriteObject(string.Format("Wartość SwitchParam: {0}. Jest null {1}", SwitchParam, SwitchParam == null));

        }

    }
}
