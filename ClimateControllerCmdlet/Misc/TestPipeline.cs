﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Management.Automation;
using System.Net;
using Microsoft.Win32;
using System.Collections.Specialized;
using System.Globalization;
using Newtonsoft.Json;
using Wiechecki.ClimateController;

#pragma warning disable 1591
namespace Wiechecki.ClimateControllerCmdlet.Examples
{
    [Cmdlet(VerbsDiagnostic.Test, "Pipeline")]
    public class TestPipeline : Cmdlet
    {
        [Parameter()]
        public SwitchParameter BooleanTrue;
        [Parameter()]
        public SwitchParameter BooleanFalse;
        private void Post(string data)
        {
            using (WebClient client = new WebClient())
            {
                NameValueCollection item = new NameValueCollection
                {
                    { "json", data }
                };
                byte[] response = client.UploadValues("http://wiechecki.it/test.php", item);
                string result = System.Text.Encoding.UTF8.GetString(response);
                WriteObject(result);
            }
        }
        protected override void ProcessRecord()
        {
            ControllerHumidityConfiguration ch = new ControllerHumidityConfiguration();
            string s = JsonConvert.SerializeObject(ch);
            WriteObject(s);
            Post(JsonConvert.SerializeObject(ch));
            ch.MinHumidity = 23.34;
            WriteObject(JsonConvert.SerializeObject(ch));
            Post(JsonConvert.SerializeObject(ch));
            ch.MaxHumidity = 77.88;
            WriteObject(JsonConvert.SerializeObject(ch));
            Post(JsonConvert.SerializeObject(ch));
        }
    }
}
