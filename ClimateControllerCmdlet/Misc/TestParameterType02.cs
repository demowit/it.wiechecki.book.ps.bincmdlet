﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
#pragma warning disable 1591
namespace it.wiechecki.book.ps.bincmdlet.test
{
    [Cmdlet(VerbsDiagnostic.Test, "ParameterType02")]
    public class TestParameterType02 : Cmdlet
    {
        [Parameter()]
        public Object ObjectParam { get; set; }
        [Parameter()]
        public Double? DoubleParam { get; set; }
        [Parameter()]
        public int? IntParam { get; set; }
        protected override void ProcessRecord()
        {
            if(ObjectParam is PSObject)
            {
                WriteObject("Is PSObject");
            }
            if(ObjectParam != null)
            {
                WriteObject(string.Format("Typ parametru ObjectParam {0}", ObjectParam.GetType().ToString()));
                WriteObject(ObjectParam.GetType().GetNestedTypes(), true);
            }            
            if(DoubleParam != null)
            {
                WriteObject(string.Format("Typ parametru DoubleParam {0}", DoubleParam.GetType().ToString()));
            }
            if(IntParam != null)
            {
                WriteObject(string.Format("Typ parametru IntParam {0}", IntParam.GetType().ToString()));
            }
            
        }

    }
}
