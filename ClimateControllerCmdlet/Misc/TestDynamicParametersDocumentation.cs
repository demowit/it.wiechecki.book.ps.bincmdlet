﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

namespace it.wiechecki.book.ps.bincmdlet.Misc
{
    #pragma warning disable 1591
    public class ClassOne
    {
        [Parameter(Mandatory = true)]
        public int Id { get; set; }
        [Parameter(ValueFromPipeline = true)]
        public string Description { get; set; }
    }

    public class ClassTwo
    {
        [Parameter(Mandatory = true)]
        public int No { get; set; }
        [Parameter()]
        public string Location { get; set; }
    }
    /// <summary>
    /// <para type="description">Przykład dokumentowania parametrów dynamicznych.</para>
    /// <para type="description">
    /// Typ DynamicParameter informuje, że parametr jest parametrem dynamicznym, dostępnym dla
    /// użytkownika po spełnieniu okreslonych warunków.
    /// </para>
    /// <example>
    /// <para>To jest przykład z nagłowka cmdletu</para>
    /// <code>Kod z nagłowka cmdletu</code>
    /// </example>
    /// <para type="link">Tu jest link z nagłówka</para>
    /// </summary>
    [Cmdlet(VerbsDiagnostic.Test, "Help")]
    public class TestDynamicParametersDocumentation : Cmdlet, IDynamicParameters
    {
        /// <summary>
        /// <para type="description">Dane wejściowe</para>
        /// <example>
        /// <para>To jest przykład z parametru InputObject cmdletu</para>
        /// <code>Kod InputObject</code>
        /// </example>
        /// <para type="link">Tu jest link z InputObject</para>
        /// </summary>
        [Parameter(ValueFromPipeline = true)]
        public Object InputObject { get; set; }
        /// <summary>
        /// <para type="description">Zestaw wynamiczny One</para>
        /// </summary>
        [Parameter()]
        public SwitchParameter SetOne { get; set; }
        /// <summary>
        /// <para type="description">Zestaw wynamiczny Two</para>
        /// </summary>
        [Parameter()]
        public SwitchParameter SetTwo { get; set; }
        #region Opis parametrów dynamicznych.
#if Makedoc
        public class DynamicParameter { }
        /// <summary>
        /// <para type="description">
        /// Parametr dynamiczny, rzeczywisty typ int. Parametr dostępny gdy wybrano SetOne.
        /// </para>
        /// <para type="description">Identyfikator zasobu.</para>
        /// </summary>
        [Parameter(Mandatory = true)]
        public DynamicParameter Id { get; set; }
        /// <summary>
        /// <para type="description">
        /// Parametr dynamiczny, rzeczywisty typ string. Parametr dostępny gdy wybrano SetOne. 
        /// </para>
        /// <para type="description">Opis zasobu.</para>
        /// </summary>
        [Parameter(ValueFromPipeline = true)]
        public DynamicParameter Description { get; set; }
        /// <summary>
        /// <para type="description">
        /// Parametr dynamiczny, rzeczywisty typ int. Parametr dostępny gdy wybrano SetTwo.
        /// </para>
        /// <para type="description">Numer lokalizacji</para>
        /// </summary>
        [Parameter(Mandatory = true)]
        public DynamicParameter No { get; set; }
        /// <summary>
        /// <para type="description">
        /// Parametr dynamiczny, rzeczywisty typ string. Parametr dostępny gdy wybrano SetTwo.
        /// </para>
        /// <para type="description">Nazwa lokalizacji.</para>
        /// </summary>
        [Parameter()]
        public DynamicParameter Location { get; set; }
#endif
        #endregion
        private ClassOne classOne = null;
        private ClassTwo classTwo = null;

        public Object GetDynamicParameters()
        {
            if (SetOne && SetTwo)
            {
                return null;
            }
            if (SetOne)
            {
                classOne = new ClassOne();
                return classOne;
            }
            if (SetTwo)
            {
                classTwo = new ClassTwo();
                return classTwo;
            }
            return null;
        }
        protected override void ProcessRecord()
        {
            
        }
    }
}
