﻿using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Text.RegularExpressions;
#pragma warning disable 1591
namespace it.wiechecki.book.ps.bincmdlet.Misc
{
    /// <summary>
    /// Klasa słyży do walidacji zarkresu wprowadzonych danych w przypadku zadeklarowania typu parametru jako Object lub Object[].
    /// W takim przypadku Powershell, otrzymany w linii poleceń łańcuch traktuje jako:
    /// 1)typ int jeśli "^[1-9]?[0-9]*$
    /// 2)typ PSobject jesli "^[1-9]?[0-9][.]?[0-9]*$
    /// 3)typ Scriptblock jesli "^\{.\}$
    /// 4)typ Hashtable jesli "^@\{[A-Za-z]?[A-Za-z0-9]*[=]{1}\"{1}.*\}{1};[A-Za-z]?[A-Za-z0-9]*=\{{1}.*\}\}$"
    /// 5)typ string w pozostałych przypadkach.
    ///     a) liczba int lub double jest traktowana jak string gdy "^[-]?[1-9]?[0-9][.]?[0-9]*$"
    ///         W tym przypadku kropkę w łańcuchu należy zastąpić separatorem dziesiętnym, pobranym z
    ///         ustawień kultury systemu.
    /// </summary>
    public class ValidateMyParamAttribute : ValidateArgumentsAttribute
    {
        private void CheckDoubleData(Object data)
        {
            if ((Convert.ToDouble(data) < Convert.ToDouble(this.min)) || (Convert.ToDouble(data) > Convert.ToDouble(this.max)))
            {
                throw new Exception("Argument out of range");
            }
        }

        private void ValidateData(object item)
        {
            if ((item == null) || (item is ScriptBlock) || (item is Hashtable))
            {
                return;
            }

            if ((item != null) && (item is int))
            {
                if((this.min is int) || (this.max is int) || (this.min is double) || (this.max is double) || (this.min is float) || (this.max is float) || (this.min is decimal) || (this.max is decimal))
                {
                    CheckDoubleData(item);
                }
                return;
            }
            if ((item != null) && (item is PSObject))
            {
                if((((PSObject)item).BaseObject != null) && ((((PSObject)item).BaseObject is Int64) || (((PSObject)item).BaseObject is UInt64) || (((PSObject)item).BaseObject is double) || (((PSObject)item).BaseObject is float) || (((PSObject)item).BaseObject is decimal)))
                {
                    Object data = ((PSObject)item).BaseObject;
                    CheckDoubleData(data);
                }
                return;
            }
            if ((item != null) && (item is string))
            {
                Match match = Regex.Match(item as string, "^[-]?[1-9]{1}[0-9]*[.]?[0-9]*$");//sprawdzamy czy wprowadzono liczbę ujemną z częścią dziesiętną
                if (match.Success)
                {
                    item = (item as string).Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
                    CheckDoubleData(item);
                    return;
                }
                if (!((string.Compare((string)this.min, (string)item, true) < 0) && (string.Compare((string)this.max, (string)item, true) > 0)))
                {
                    throw new Exception("Data out of range");
                }
                return;
            }
            return;
        }
        protected override void Validate(object arguments, EngineIntrinsics engineIntrinsics)
        {
            if (!this.min.GetType().Equals(this.max.GetType()))
            {
                throw new ValidationMetadataException();
            }
            if(arguments is Object[])
            {
                Object[] objcolection = arguments as Object[];
                foreach (Object obj in objcolection)
                {
                    ValidateData(obj);
                }
            }
            else
            {
                ValidateData(arguments);
            }            
        }
        private Object min;
        private Object max;
        public ValidateMyParamAttribute(Object min, Object max)
        {
            this.min = min;
            this.max = max;                
        }
    }
    [Cmdlet(VerbsDiagnostic.Test, "Validate")]
    public class TestParameterAttribute : Cmdlet
    {
        [Parameter()]
        [ValidateMyParam("10:00:12", "23:00:00")]
        //[ValidateRange("00:00:00",-12)]
        public Object MyParam { get;set;}
        [Parameter()]
        [ValidateRange(-10, 10)]
        public Object MyParam1 { get; set; }
        protected override void ProcessRecord()
        {
            WriteObject(string.Format("Typ parametru {0}", MyParam1.GetType().ToString()));
        }
    }
}
