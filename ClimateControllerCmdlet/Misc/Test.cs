﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

namespace it.wiechecki.book.ps.bincmdlet.Misc
{
    [Cmdlet(VerbsDiagnostic.Test, "Test")]
    public class Test : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public Object MyObject { get; set; }
        [Parameter()]
        public float?[] MyFloat { get; set; }
        [Parameter()]
        public Int32?[] MyInt { get; set; }
        [Parameter()]
        public string[] MyString { get; set; }
        public enum MyEnumType { one, two}
        [Parameter()]
        public MyEnumType? MyEnum { get; set; }
        [Parameter()]
        public SwitchParameter SetMe { get; set; }
        [Parameter()]
        public bool? BoolSetMe { get; set; }
        protected override void ProcessRecord()
        {
            if (MyObject is PSObject)
            {
                WriteObject(string.Format("MyObject: {0} IsNull {1} Type: PSObject and {2}", (MyObject as PSObject).BaseObject, MyObject == null, (MyObject != null) ? (MyObject as PSObject).BaseObject.GetType().ToString() : "unknown"));
            }
            else
            {
                WriteObject(string.Format("MyObject: {0} IsNull {1} Type: {2}", MyObject, MyObject == null, (MyObject != null) ? MyObject.GetType().ToString() : "unknown"));
            }
            
            WriteObject(string.Format("MyFloat: {0} IsNull {1} Type: {2}", MyFloat, MyFloat == null, (MyFloat != null) ? MyFloat.GetType().ToString() : "unknown"));
            WriteObject(string.Format("MyInt: {0} IsNull {1} Type: {2}", MyInt, MyInt == null, (MyInt != null) ? MyInt.GetType().ToString() : "unknown"));
            WriteObject(string.Format("MyString: {0} IsNull {1} Type: {2}", MyString, MyString == null, (MyString != null) ? MyString.GetType().ToString() : "unknown"));
            WriteObject(string.Format("MyEnum: {0} IsNull {1} Type: {2}", MyEnum, MyEnum == null, (MyEnum != null) ? MyEnum.GetType().ToString() : "unknown"));
            WriteObject(string.Format("SetMe: {0} IsNull {1} Type: {2}", SetMe, SetMe == null, (SetMe != null) ? SetMe.GetType().ToString() : "unknown"));
            WriteObject(string.Format("BoolSetMe: {0} IsNull {1} Type: {2}", BoolSetMe, BoolSetMe == null, (BoolSetMe != null) ? BoolSetMe.GetType().ToString() : "unknown"));
        }
    }
}
