﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Reflection;
using System.IO;

namespace testcodeproperty
{
    public class CodeProperty
    {
        public static Object GetArea(PSObject instance)
        {
            double d = Convert.ToDouble(instance.Properties["Radius"].Value);
            return ( d * 2 * 3.14);
        }

        public static void SetArea(PSObject instance, Object value)
        {
            instance.Properties["Radius"].Value = Convert.ToDouble(value) / 2 / 3.14;
        }
    }
}

namespace it.wiechecki.book.ps.bincmdlet.Misc
{
    [Cmdlet(VerbsDiagnostic.Test, "CodeProperty")]
    public class TestPSCodeProperty : Cmdlet 
    {
        [Parameter(ValueFromPipeline = true)]
        public Double Radius { get; set; } = 10;
        [Parameter()]
        public SwitchParameter FromCmdlet { get; set; }
        [Parameter()]
        public SwitchParameter ExternalModule { get; set; }
        [Parameter()]
        public string ClassName { get; set; }
        public static Object GetArea(PSObject instance)
        {
            double d = Convert.ToDouble(instance.Properties["Radius"].Value);
            return (d * 2);
        }
        public static void SetArea(PSObject instance, Object value)
        {
            instance.Properties["Radius"].Value = Convert.ToDouble(value) / 2;
        }
        protected override void ProcessRecord()
        {
            MethodInfo getter = null;
            MethodInfo setter = null;
            if (FromCmdlet)
            {
                getter = typeof(it.wiechecki.book.ps.bincmdlet.Misc.TestPSCodeProperty).GetMethod("GetArea");
                setter = typeof(it.wiechecki.book.ps.bincmdlet.Misc.TestPSCodeProperty).GetMethod("SetArea");            
            }
            else
            {
                if (ExternalModule)
                {
                    Assembly asm = Assembly.LoadFile(Environment.CurrentDirectory + "\\PluginExample.dll");
                    Type tp = asm.GetType("PluginExample." + ClassName);
                    if (tp != null)
                    {
                        getter = tp.GetMethod("GetArea");
                        setter = tp.GetMethod("SetArea");
                    }
                }
                else
                {
                    getter = typeof(testcodeproperty.CodeProperty).GetMethod("GetArea");
                    setter = typeof(testcodeproperty.CodeProperty).GetMethod("SetArea");
                }
            }
            PSCodeProperty cp = new PSCodeProperty("Area", getter, setter);
            PSObject obj = new PSObject();
            obj.Properties.Add(new PSNoteProperty("Radius", Radius));
            obj.Properties.Add(cp);
            WriteObject(obj);
        }
    }
}
