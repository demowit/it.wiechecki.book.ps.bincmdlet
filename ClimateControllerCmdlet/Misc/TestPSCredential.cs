﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Runtime.InteropServices;
using System.Security;

namespace it.wiechecki.book.ps.bincmdlet.Misc
{
    [Cmdlet(VerbsDiagnostic.Test, "Credential")]
    public class TestPSCredential : Cmdlet
    {
        [Parameter(Mandatory = true)]
        public PSCredential Credential { get; set; }
        public static string PrintPassword(SecureString password)
        {
            // Uncrypt the password and get a reference to it...
            IntPtr bstr = Marshal.SecureStringToBSTR(password);

            try
            {
                // Printing the uncrypted password...
                return Marshal.PtrToStringBSTR(bstr);
            }

            finally
            {
                Marshal.ZeroFreeBSTR(bstr);
            }
        }
        protected override void ProcessRecord()
        {
            WriteObject(string.Format("{0}", Credential.UserName));
            WriteObject(string.Format("{0}", Credential.Password));
            WriteObject(string.Format("{0}", Credential.GetNetworkCredential().Password));
            WriteObject(string.Format("{0}", Credential.GetNetworkCredential().GetHashCode()));
            WriteObject(string.Format("{0}", PrintPassword(Credential.Password)));
        }
    }
}
