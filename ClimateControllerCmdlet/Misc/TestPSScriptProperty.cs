﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

namespace it.wiechecki.book.ps.bincmdlet.Misc
{
    [Cmdlet(VerbsDiagnostic.Test, "ScriptProperty")]
    public class TestScriptProperty : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public Double Radius { get; set; } = 10;
        protected override void ProcessRecord()
        {
            PSObject obj = new PSObject();
            obj.Properties.Add(new PSNoteProperty("Radius", Radius));
            ScriptBlock getter = ScriptBlock.Create("$this.Radius*2*3.14");
            ScriptBlock setter = ScriptBlock.Create("param([double]$value);$this.Radius=$value/2/3.14");
            obj.Properties.Add(new PSScriptProperty("Area", getter, setter));
            WriteObject(obj);
        }
    }
}
