﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Management.Automation;

namespace it.wiechecki.book.ps.bincmdlet.Misc
{

    public class ScriptBlockAttribute : ArgumentTransformationAttribute
    {
        protected ScriptBlock RemoveAlias(ScriptBlock scriptBlock)
        {
            string s = scriptBlock.ToString().Replace("$_", "$args[0]");
            string str = Regex.Replace(s, "[$][P|p][S|s][I|i][T|t][E|e][M|m]", "$args[0]");
            return ScriptBlock.Create(str);
        }

        public override object Transform(EngineIntrinsics engineIntrinsics, object inputData)
        {
            object input = inputData;
            if (input is PSObject)
            {
                input = ((PSObject)input).BaseObject;
            }
            if (input is ScriptBlock)
            {
                return RemoveAlias(input as ScriptBlock);
            }
            if (input is IList)
            {
            }
            return inputData;
        }
    }
    [Cmdlet(VerbsDiagnostic.Test, "ScriptBlock")]
    public class TestScriptBlock : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public Object InputObject { get; set; }
        [Parameter()]
        [ScriptBlock()]
        public Object MyParam { get; set; }
        [Parameter()]
        public int Param1 { get; set; }
        [Parameter()]
        public string Param2 { get; set; }
        

        protected override void ProcessRecord()
        {
            //WriteObject(Args)
            if(MyParam is ScriptBlock)
            {
                //WriteObject(InputObject);
                var result = ((ScriptBlock)MyParam).Invoke(InputObject)[0];
                WriteObject(result);
                return;
            }
            WriteObject(MyParam);
            WriteObject(Param2);
        }
    }
}
