﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Reflection;

namespace testmethods
{
    public class MethodClass
    {
        public static string CodeMethod(PSObject instance, string name, int value)
        {
            return string.Format("Obiekt z CodeMethod {0} {1}", name, value);
        }
        public static string CodeMethodB(PSObject instance, string name)
        {
            return string.Format("Obiekt z overload CodeMethod {0}", name);
        }
    }
}

namespace it.wiechecki.book.ps.bincmdlet.Misc
{
    [Cmdlet(VerbsDiagnostic.Test, "Method")]
    public class TestPSMethod : Cmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public Object InputObject { get; set; }
        [Parameter(ParameterSetName = "CodeMethod params")]
        public string Name { get; set; }
        [Parameter(ParameterSetName = "CodeMethod params")]
        public int Value { get; set; }
        [Parameter(ParameterSetName = "CodeMethod")]
        public SwitchParameter CodeMethod { get; set; }
        [Parameter(ParameterSetName = "Script Method")]
        public SwitchParameter ScriptMethod { get; set; }
        [Parameter(ParameterSetName = "Script Method")]
        public ScriptBlock Script { get; set; }
        [Parameter(ParameterSetName = "Hash Method")]
        public Hashtable Hash { get; set; }
        private Object GetKeyValue(string fullKeyName, DictionaryEntry dictionaryEntry, object inputObject)
        {

            return inputObject ?? ((fullKeyName.IndexOf(dictionaryEntry.Key.ToString().ToLower()) == 0) ? dictionaryEntry.Value : null);
        }
        protected override void ProcessRecord()
        {
            if(Hash != null)
            {
                foreach (DictionaryEntry de in Hash)
                {                    
                    WriteObject(string.Format("{0} {1} {2}", de.Key.ToString(), de.Value, de.Value.GetType().ToString()));
                }
                return;
            }
            if (InputObject != null)
            {
                var s = (InputObject as PSObject).Methods["CodeMethod"].Invoke(Name, Value);
                WriteObject(s);
                return;
            }
            PSObject obj = new PSObject();
            if (CodeMethod)
            {                
                MethodInfo mi = typeof(testmethods.MethodClass).GetMethod("CodeMethod");
                obj.Methods.Add(new PSCodeMethod("CodeMethod", mi));
                mi = typeof(testmethods.MethodClass).GetMethod("CodeMethodB");
                obj.Methods.Add(new PSCodeMethod("CodeMethod", mi));
            }
            if (ScriptMethod)
            {
                obj.Methods.Add(new PSScriptMethod("ScriptMethod", Script));
            }
            WriteObject(obj);
        }
    }
}
