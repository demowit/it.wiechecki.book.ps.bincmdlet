﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

namespace PluginA
{
    public class Class01
    {
        public static string Method01(PSObject instance, string propertyName)
        {
            return string.Format("PluginA.Class01.Method01.Parameter {0}={1}", propertyName, instance.Properties[propertyName].Value);
        }

        public static string Method02(PSObject instance, string propertyNameA, string propertyNameB)
        {
            return string.Format("PluginA.Class01.Method012Parameter {0}={1} {2}={3}", propertyNameA, instance.Properties[propertyNameA].Value, propertyNameB, instance.Properties[propertyNameB].Value);
        }
    }

    public class Class02
    {
        public static string Method01(PSObject instance, string propertyName)
        {
            return string.Format("PluginA.Class02.Method01.Parameter {0}={1}", propertyName, instance.Properties[propertyName].Value);
        }

        public static string Method02(PSObject instance, string propertyNameA, string propertyNameB)
        {
            return string.Format("PluginA.Class02.Method012Parameter {0}={1} {2}={3}", propertyNameA, instance.Properties[propertyNameA].Value, propertyNameB, instance.Properties[propertyNameB].Value);
        }
    }

}
