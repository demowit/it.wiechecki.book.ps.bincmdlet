﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wiechecki.ClimateController;
using Wiechecki.CrudApi;
using Newtonsoft.Json;
using Microsoft.Win32;

namespace Wiechecki.InternetControllerCommand
{
    public sealed class RestReply
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public int DatabaseErrorCode { get; set; }
        public Object Data { get; set; } = null;
        public T ToObject<T>()
        {
            return JsonConvert.DeserializeObject<T>(this.Data.ToString());
        }
    }
    public class InternetPluginMessage : PluginMessage
    {
        public InternetPluginMessage(string methodName = null, RestReply restReply = null)
        {
            if ((methodName == null) && (restReply == null))
            {
                this.DebugMessage = Properties.Resources.DebugNullMethod;
                this.VerboseMessage = Properties.Resources.VerboseNullMethod;
            }
            else if ((methodName != null) && (restReply == null))
            {
                this.DebugMessage = string.Format(Properties.Resources.DebugWithMethod, methodName);
                this.VerboseMessage = string.Format(Properties.Resources.VerboseWithMethod, methodName);
            }
            else if ((methodName != null) && (restReply != null))
            {
                this.DebugMessage = string.Format(Properties.Resources.DebugWithMethodRestReply, methodName, restReply.ErrorCode, restReply.ErrorMessage, restReply.DatabaseErrorCode);
                this.VerboseMessage = string.Format(Properties.Resources.VerboseWithMethodRestReply, methodName, restReply.ErrorCode, restReply.ErrorMessage, restReply.DatabaseErrorCode);
            }
            else
            {
                this.DebugMessage = Properties.Resources.DebugNullMethod;
                this.VerboseMessage = Properties.Resources.VerboseNullMethod;
            }
        }
    }
    public class ControllerCommand : IControllerCommand
    {
        private static string homeUriPattern = "http://api.wiechecki.it/ctrl/v1/controller.php";
        private const string UserIdValueName = "UserId";
        private static string controllerRegistryKeyName = @"software\wiechecki.it\InternetControllerCommand";
        private static void CheckKey()
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(controllerRegistryKeyName, true);
            if (regKey == null)
            {
                regKey = Registry.CurrentUser.CreateSubKey(controllerRegistryKeyName, RegistryKeyPermissionCheck.ReadWriteSubTree);
            }
            if (regKey.GetValue(UserIdValueName) == null)
            {
                string guid = string.Format("PS{0}", Guid.NewGuid().ToString());
                guid = guid.Replace("-", string.Empty);
                regKey.SetValue(UserIdValueName, guid);
            }
            regKey.Close();
        }
        public static string UserId
        {
            get
            {
                CheckKey();
                RegistryKey regKey = Registry.CurrentUser.OpenSubKey(controllerRegistryKeyName);
                string guid = string.Empty;
                guid = regKey.GetValue(UserIdValueName).ToString();
                return guid;
            }
        }
        public static string PluginName = "InternetController";
        public string GetPluginName()
        {
            return PluginName;
        }
        public void SetControllerRegistryKeyName(string keyName = null)
        {
            controllerRegistryKeyName = keyName;
        }
        public void Register(Action<PluginMessage> action)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            RestReply reply = sca.Create<RestReply, string>(string.Format("{0}/none/register", UserId), UserId);
            action?.Invoke(new InternetPluginMessage("Register"));
        }
        public ControllerData[] GetData(string ctrlAddress, Action<PluginMessage> action = null)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            RestReply reply = sca.Read<RestReply>(string.Format("{0}/{1}/readout", UserId, ctrlAddress));
            action?.Invoke(new InternetPluginMessage("GetData", reply));
            return (reply.Data != null) ? reply.ToObject<ControllerData[]>() : null;
        }
        public ControllerSettings[] GetSetting(string ctrlAddress, Action<PluginMessage> action = null)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            RestReply reply = sca.Read<RestReply>(string.Format("{0}/{1}/setting", UserId, ctrlAddress));
            action?.Invoke(new InternetPluginMessage("GetSetting", reply));
            return (reply.Data != null) ? reply.ToObject<ControllerSettings[]>() : null;
        }
        public void UpdateSettings(string ctrlAddress, ControllerSettings controllerSettings, Action<PluginMessage> action = null)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            RestReply reply = sca.Update<RestReply, ControllerSettings>(string.Format("{0}/{1}/setting", UserId, ctrlAddress), controllerSettings);
            action?.Invoke(new InternetPluginMessage("UpdateSetting", reply));
        }
        public ControllerState[] GetState(string ctrlAddress, Action<PluginMessage> action = null)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            RestReply reply = sca.Read<RestReply>(string.Format("{0}/{1}/state", UserId, ctrlAddress));
            action?.Invoke(new InternetPluginMessage("GetState", reply));
            return (reply.Data != null) ? reply.ToObject<ControllerState[]>() : null;
        }
        public Period[] GetCurve(string ctrlAddress, int? periodId = null, Action<PluginMessage> action = null)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            string command = (periodId == null) ? string.Format(string.Format("{0}/{1}/period", UserId, ctrlAddress)) : string.Format(string.Format("{0}/{1}/period/{2}", UserId, ctrlAddress, periodId));
            RestReply reply = sca.Read<RestReply>(command);
            action?.Invoke(new InternetPluginMessage("GetCurve", reply));
            return (reply.Data != null) ? reply.ToObject<Period[]>() : null;
        }
        public void UpdatePeriod(string ctrlAddress, Period period, Action<PluginMessage> action = null)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            RestReply reply = sca.Update<RestReply, Period>(string.Format("{0}/{1}/period/{2}", UserId, ctrlAddress, period.PeriodId), period);
            action?.Invoke(new InternetPluginMessage("UpdatePeriod", reply));
        }
        public void DeletePeriod(string ctrlAddress, int periodId, Action<PluginMessage> action = null)
        {
            SimpleCrudApi sca = new SimpleCrudApi(homeUriPattern);
            RestReply reply = sca.Delete<RestReply>(string.Format("{0}/{1}/period/{2}", UserId, ctrlAddress, periodId));
            action?.Invoke(new InternetPluginMessage("DeletePeriod", reply));
        }
    }
}
