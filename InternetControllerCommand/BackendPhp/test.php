<?php

include_once('common/httpcommand.php');

class TestCommand extends HttpCommand{
    public $CommandOne;
    function __construct($source){
        parent::__construct($source);
        $this->CommandOne = strtolower($this->Parts[0]);
    }
}

// define('ConstOne', 'readout');
// define('ConstTwo', 'state');

class CommandExecutor{
    const ConstOne = 'readout';
    const ConstTwo = 'state';
    public function ExecuteFirst($command){
        $cmd = new TestCommand($command);
        echo 'Execute first <br />';
        echo "To jest komenda: ".$cmd->CommandOne.'</br>';
        switch ($cmd->CommandOne){
            case 'readout':
                echo 'From switch ConstOne: '.$cmd->CommandOne.'<br />';
                break;
            case 'state':
                echo 'From switch ConstTwo: '.$cmd->CommandOne.'<br />';
                break;
            default:
                echo 'From switch default: '.$cmd->CommandOne.'<br />';
                break;
        }
    }

    public function ExecuteSecond($command){
        $cmd = new TestCommand($command);
        echo 'Execute second <br />';
        echo "To jest komenda: ".$cmd->CommandOne.'<br />';
        switch ($cmd->CommandOne){
            case self::ConstOne:
                echo 'From switch ConstOne: '.$cmd->CommandOne.'<br />';
                break;
            case self::ConstTwo:
                echo 'From switch ConstTwo: '.$cmd->CommandOne.'<br />';
                break;
            default:
                echo 'From switch default: '.$cmd->CommandOne.'<br />';
                break;
        }
    }
}

$ce = new CommandExecutor();
$ce->ExecuteFirst($_SERVER['PATH_INFO']);
$ce->ExecuteSecond($_SERVER['PATH_INFO']);

$geo = (file_get_contents('http://www.geoplugin.net/json.gp?ip='.$_SERVER['REMOTE_ADDR']));
$data = json_decode($geo);
$country = $data->{'geoplugin_countryName'};
$countryCode = $data->{'geoplugin_countryCode'};
$city = $data->{'geoplugin_city'};
$UserId = 'User';
$query = 'call jarekww_sym.RegisterUser("'.$UserId.'","'.$country.'","'.$countryCode.'","'.$city.'")';

echo $geo.'<br />';
echo $query.'<br />';