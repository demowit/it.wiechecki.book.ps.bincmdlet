<?php
header('Access-Control-Allow-Origin: *');

/*
    Tabele:
        readout - tabela zawiera zapisane odczyty temperatury i wilgotności. Użytkownik: pobieranie. Symulator: Zmiana
            id, userid, ctrlid, readoutid, readout_timestamp, temperature, humidity
            maksymalna ilość wierszy to ilość użytkowników * ilość kontrolerów * 20
            Format zapytania:
                get - address/controller.php/userid/ctrlid/readout zwraca całą tablicę. Nie można zwrócić tylko jednego odczytu

            Sql query:
                select ctrlid, readoutid, readout_timestamp, temperature, humidity from readout where userid = 'userid' and ctrlid = 'ctrlid' order by readoutid;
        
        period - pobieranie danych z tabeli opisującej okresy. Użytkownik: dodawanie, zmiana, usuwanie, pobieranie
            id, userid, ctrlid, periodid, period_available, start_period_time, finish_period_time, min_temperature, max_temperature
            maksymalna ilość wierszy to ilość użytkowników * ilość kontrolerów * 25
            Format zapytania:
                get - address/userid/ctrlid/period
                put - address/userid/ctrlid/period/periodid
                delete - address/userid/ctrlid/period/periodid
        
        state - tabela zawiera stan kontrolera. Użytkownik: pobieranie. Symulator: zmiana, dodawanie usera.
            id, userid, ctrlid, air_conditioner_state, heating_state, humidifcation_state
            maksymalna ilość wierszy to ilość użytkowników * ilość kontrolerów
            Format zapytania:
                get - address/userid/ctrlid/state

        setting - tabela zawiera ustawienia kontrolera. Użytkownik: odczyt, zmiana, usunięcie
            id, userid, ctrlid, min_temperature, max_temperature, min_humidity, max_humidity
            maksymalna ilość wierszy to ilość użytkowników * ilość kontrolerów
            Format zapytania:
                get - address/userid/ctrlid/setting. Zwraca dane w json
                put - address/userid/ctrlid/setting. Wysyła dane w json
                delete address/userid/ctrlid/setting. Przywrócenie danych domyślnych

        ctrl_loop - tabela zawiera informacje o aktualnym numerze wpisu w tabeli readout. Tabela wewnętrzna. Użytkownik: nic. Symulator: zmiana, dodawanie
            id, userid, ctrlid, loopid
            maksymalna ilość wierszy to ilość użytkowników * ilość kontrolerów

        user - tabela zawiera informacje o zarejestrowanych użytkownikach. Użytkownik: POST. Symulator: dodanie i usunięcie po okresie bezczynności.
            id, userid, last_access_time
            maksymalna ilość wierszy: nielimitowana
                post - address/userid/register

        Ogólna postać zapytania:
            address/userid/ctrlid/table/item_idx
        gdzie address to:
            api.wiechecki.it/ctrl/v1/controller.php
        
        Format odpowiedzie: {ErrorOk: int; ErrorMessage: string; Data: json_string}
*/
include("config.php");
$method = $_SERVER['REQUEST_METHOD'];
$request = $_SERVER['PATH_INFO'];

$ce = new CtrlCrud($dbHost, $dbUser, $dbPassword, $request);
$cmdResult = $ce->ExecuteHttpMethod($method);
echo $cmdResult->ToJson();