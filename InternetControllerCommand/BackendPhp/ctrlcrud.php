<?php

class CtrlCrud extends BaseCrud{

    const Readout = 'readout';
    const Period = 'period';
    const State = 'state';
    const Setting = 'setting';
    const Register = 'register';
    // userid/ctrlid/readout
    // userid/ctrlid/period/periodid
    // userid/ctrlid/state
    // userid/ctrlid/setting    
    protected function Read(){        
        switch ($this->cmd->Command){
            case self::Readout:
                $query = 'select CtrlId, ReadoutId, ReadoutTimeStamp, Temperature, Humidity from jarekww_sym.Readout where UserId="'.$this->cmd->UserId.'" and CtrlId="'.$this->cmd->CtrlId.'"';
                break;
            case self::Period:
                if (isset($this->cmd->Index)){
                    $query = 'select PeriodId, PeriodAvailable, StartPeriodTime, FinishPeriodTime, MinTemperature, MaxTemperature from jarekww_sym.Period where UserId="'.$this->cmd->UserId.'" and CtrlId="'.$this->cmd->CtrlId.'" and PeriodId='.$this->cmd->Index;
                }
                else{
                    $query = 'select PeriodId, PeriodAvailable, StartPeriodTime, FinishPeriodTime, MinTemperature, MaxTemperature from jarekww_sym.Period where UserId="'.$this->cmd->UserId.'" and CtrlId="'.$this->cmd->CtrlId.'"';
                }
                break;
            case self::State:
                $query = 'select CtrlId, AirConditionerState, HeatingState, HumidificationState from jarekww_sym.State where UserId="'.$this->cmd->UserId.'" and CtrlId="'.$this->cmd->CtrlId.'"';
                break;
            case self::Setting:
                $query = 'select * from jarekww_sym.Setting where  UserId="'.$this->cmd->UserId.'" and CtrlId="'.$this->cmd->CtrlId.'"';
                break;
            default:
                return $this->DefaultError(404, code404.'Method Read Command: '.$this->cmd->Source);
        }
        
        return $this->ExecuteSqlQuery($query);
    }
    private function Register(){
        $geo = json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip='.$_SERVER['REMOTE_ADDR']));
        $country = $geo->{'geoplugin_countryName'};
        $countryCode = $geo->{'geoplugin_countryCode'};
        $city = $geo->{'geoplugin_city'};
        $query = 'call jarekww_sym.RegisterUser("'.$this->cmd->UserId.'","'.$country.'","'.$countryCode.'","'.$city.'")';        
        return $this->ExecuteSqlQuery($query);
    }

    protected function Create(){
        //$data = file_get_contents('php://input');
        switch ($this->cmd->Command){
            case self::Register:
                return $this->Register();
            default:
                return $this->DefaultError(404, code404.'Method Create Command: '.$this->cmd->Source);
        }
        
    }
    // userid/ctrlid/period/periodid
    // userid/ctrlid/setting
    protected function Update(){
        $data = json_decode(file_get_contents('php://input'));
        switch ($this->cmd->Command){
            case self::Period:
                if (ClassPropertyValidator::Validate($data, ['StartPeriodTime', 'FinishPeriodTime', 'MinTemperature', 'MaxTemperature'])){
                    $query = 'call jarekww_sym.UpdatePeriod("'.$this->cmd->UserId.'","'.$this->cmd->CtrlId.'",'.$this->cmd->Id.','.$data->{'StartPeriodTime'}.','.$data->{'FinishPeriodTime'}.','.$data->{'MinTemperature'}.','.$data->{'MaxTemperature'}.')';
                    return $this->ExecuteSqlQuery($query);
                }
                return $this->DefaultError(404, code404.'Method Update Command: '.$this->cmd->Source.' no correct data');
            case self::Setting:
                if (ClassPropertyValidator::Validate($data, ['MinTemperature', 'MaxTemperature', 'MinHumidity', 'MaxHumidity'])){            
                    $query = 'call jarekww_sym.UpdateSetting("'.$this->cmd->UserId.'","'.$this->cmd->CtrlId.'",'.$data->{'MinTemperature'}.','.$data->{'MaxTemperature'}.','.$data->{'MinHumidity'}.','.$data->{'MaxHumidity'}.')';
                    return $this->ExecuteSqlQuery($query);
                }
                return $this->DefaultError(404, code404.'Method Update Command: '.$this->cmd->Source.' no correct data');
            default:
                return $this->DefaultError(404, code404.'Method Update Command: '.$this->cmd->Source);
        }
    }
    // userid/ctrlid/period/periodid
    protected function Delete(){
        switch ($this->cmd->Command){
            case self::Period:
                $query = 'update jarekww_sym.Period set PeriodAvailable=1, StartPeriodTime=null, FinishPeriodTime=null,MinTemperature=null,MaxTeperature=null where PeriodId='.$index;
                break;
            default:
                return $this->DefaultError(404,  code404.'Method Delete Command: '.$this->cmd->Source);
        }
        return $this->ExecuteSqlQuery($query);
    }

    function __construct($dbHost, $dbUser, $dbPassword, $request){
        parent::__construct($dbHost, $dbUser, $dbPassword, $request);
        $this->Register();
    }
}