<?php

class ClassPropertyValidator{

    public static function Validate($class, $propertyNames){
        $result = ($class != null);
        foreach($propertyNames as $propertyName){
            if (!$result) break;
            $result &= property_exists($class, $propertyName);
        }
        return $result;
    }
}