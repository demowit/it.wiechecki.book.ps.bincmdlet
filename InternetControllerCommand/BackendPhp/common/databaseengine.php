<?php

class DatabaseEngine
{
    private $host;
    private $userName;
    private $userPassword;
    public $queryResult;
    public $ErrorCode;
    function __construct($host, $userName, $userPassword){
        $this->userName = $userName;
        $this->userPassword = $userPassword;
        $this->host = $host;
    }

    public function Query($query){
        $returnResult = false;
        $this->queryResult = NULL;
        try{
            $conn = new mysqli($this->host, $this->userName, $this->userPassword);        
            if ($conn->connect_error) {
                $this->queryResult = NULL;
                $this->ErrorCode = $conn->connect_error;
                return false;
            } 
            $conn->set_charset("utf8");
            $result = $conn->query($query);        
            if ($result){
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $jsonData[] = $row;
                    }    
                    $this->queryResult = json_encode($jsonData, JSON_UNESCAPED_UNICODE);
                    $result->free();
                }
                $this->ErrorCode = $conn->errno;
                $returnResult = true;
            }else{
                $this->ErrorCode = $conn->errno;
                $this->queryResult = NULL;
                $returnResult = false;
            }
            $conn->close();    
            return $returnResult;
        }
        catch(Exception $e){
            $returnResult = false;
            $this->queryResult = null;
            $this->ErrorCode = $e->getCode();
        }
    }
}

