<?php

abstract class BaseCrud{
    private $dbHost;
    private $dbUser;
    private $dbPassword;

    protected function ExecuteSqlQuery($query){
        $commandResult = new RestCommandDatabaseResult();
        $db = new DatabaseEngine($this->dbHost, $this->dbUser, $this->dbPassword);
        if ($db->Query($query)){    
            $commandResult->ErrorCode = 200;        
            $commandResult->DatabaseErrorCode = $db->ErrorCode;
            $commandResult->ErrorMessage = code200;
            $commandResult->Data = json_decode($db->queryResult);
        }else{
            $commandResult->ErrorCode = 480;
            $commandResult->DatabaseErrorCode = $db->ErrorCode;
            $commandResult->ErrorMessage = code480;
        }
        return $commandResult;
    }

    protected function DefaultError($errorCode, $errorMessage){
        $error = new RestCommandDatabaseResult();
        $error->ErrorCode = $errorCode;
        $error->ErrorMessage = $errorMessage;
        return $error;
    }

    abstract protected function Read();
    abstract protected function Delete();
    abstract protected function Update();
    abstract protected function Create();

    public function ExecuteHttpMethod($methodName){
        
        switch($methodName){
            case 'GET':
                $cmdResult = $this->Read();
                break;
            case 'DELETE':
                $cmdResult = $this->Delete();
                break;
            case 'POST':
                $cmdResult = $this->Create();
                break;
            case 'PUT':
                $cmdResult = $this->Update();
                break;
            default:
                $cmdResult = $this->DefaultError(501, code501);        
        }
        return $cmdResult;
    }
    function __construct($dbHost, $dbUser, $dbPassword, $request){
        $this->dbHost = $dbHost;
        $this->dbUser = $dbUser;
        $this->dbPassword = $dbPassword;
        $this->cmd = new BincmdletCommand($request);
    }
}
