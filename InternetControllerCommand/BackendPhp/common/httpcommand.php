<?php 

class HttpCommand
{
    public $Parts;
    public $Source;
    private function SplitCommand($source){      
        $this->Parts = explode('/', trim($source,'/'));
    }

    function __construct($source){
        $this->Source = $source;
        $this->SplitCommand($source);
    }

    public function ReturnCommand($count){
        $result = '';
        for($i = 0; $i < $count; ++$i){
            $result .= $this->Parts;
        }

    }
}
