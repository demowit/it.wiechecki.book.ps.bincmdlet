<?php

class BincmdletCommand extends HttpCommand{
    public $UserId;
    public $CtrlId;
    public $Command;
    public $Index;
    function __construct($source){
        parent::__construct($source);
        $this->UserId = strtolower($this->Parts[0]);
        $this->CtrlId = strtolower($this->Parts[1]);
        $this->Command = strtolower($this->Parts[2]);
        if (isset($this->Parts[3])){
            $this->Index = strtolower($this->Parts[3]);
        }
    }
}